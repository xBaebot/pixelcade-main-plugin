package co.pixelcade.hub.gadgets;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Gadget {
	
	public enum GadgetType {
		
		EXAMPLE("Example", "Example thingy magigy", -1, 2000000, Material.BONE, (byte) 0);
		
		private String name, description;
		private int cost, cooldown;
		private byte data;
		private Material material;
		
		GadgetType(String name, String description, int cost, int cooldown, Material material, byte data) {
			this.name = name;
			this.description = description;
			this.cost = cost;
			this.cooldown = cooldown;
			this.data = data;
			this.material = material;
		}
		
		public String getName() {
			return name;
		} 
		
		public String getDescription() {
			return description;
		}
		
		public int getCost() {
			return cost;
		}
		
		public int getCooldown() {
			return cooldown;
		}
		
		public byte getData() {
			return data;
		}
		
		public Material getMaterial() {
			return material;
		}
	}
	
	protected String owner;
	protected GadgetType type;
	
	public static List<Gadget> activeGadgets = new ArrayList<Gadget>();
	
	public Gadget(String owner, GadgetType type) {
		this.type = type;
		this.owner = owner;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public GadgetType getGadgetType() {
		return type;
	}

	public abstract void use(Player player);
	
	public void add() {
		
		Player target = Bukkit.getPlayer(owner);
		if(target == null) return;		
		
		if(!activeGadgets.contains(this)) activeGadgets.add(this);
		target.getInventory().setItem(4, new ItemStack(type.getMaterial(), 1, type.getData()));
	}
	
	public void remove() {
		
		Player target = Bukkit.getPlayer(owner);
		if(target == null) {
			if(activeGadgets.contains(this)) activeGadgets.remove(this);
			return;	
		}
		
		if(activeGadgets.contains(this)) activeGadgets.remove(this);
		
		target.getInventory().setItem(4, null);
	}
	
	public static Gadget getGadgetByOwnerName(String name) {
		for(Gadget gadget : activeGadgets) {
			if(gadget.getOwner().equalsIgnoreCase(name)) {
				return gadget;
			}
		}
		
		return null;
	}

}