package co.pixelcade.core.api;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Created by alex on 23/08/2015.
 */
public class Title
{

	private PacketPlayOutTitle title(String text){
		return new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{'text':'" + text + "'}"));
	}

	private PacketPlayOutTitle subTitle(String text){
		return new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{'text':'" + text + "'}"));
	}

	public Title(Player player, String title, String subTitle){
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(title(title));
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(subTitle(subTitle));
	}

}
