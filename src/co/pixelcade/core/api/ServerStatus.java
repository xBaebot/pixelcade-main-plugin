package co.pixelcade.core.api;

public class ServerStatus {
	
	public enum Status{
		OFFLINE,
		STARTING,
		ONLINE,
		FULL,
		STOPPING;
	}
	
	public String server;
	public Status serverStatus;
	
	public ServerStatus(String server){
		this.server = server;
	}
	
	public Status getStatus(){
		return this.serverStatus;
	}
	
	public String getServer(){
		return this.server;
	}
}
