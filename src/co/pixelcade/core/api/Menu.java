package co.pixelcade.core.api;

import java.util.List;

import co.pixelcade.core.common.Module;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public abstract class Menu extends Module {

	private Inventory _inv;
	
	public Menu(String name, int slots){
		_inv = Bukkit.createInventory(null, slots, name);
	}
	
	public void addItem(String name, int slot, Material mat){
		ItemStack i = new ItemStack(mat);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);
		_inv.setItem(slot, i);
	}
	
	public void addItem(String name, int slot, Material mat, List<String> lore){
		ItemStack i = new ItemStack(mat);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(lore);
		i.setItemMeta(im);
		_inv.setItem(slot, i);
	}
	
	public void addGlass(String name, int slot, DyeColor dc)
	{
		ItemStack i = new ItemStack(Material.STAINED_GLASS_PANE, 1, dc.getData());
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);
		_inv.setItem(slot, i);
	}
	
	public void addGlass(int slot, DyeColor dc)
	{
		addGlass("�1 ", slot, dc);
	}
	
	public void openInventory(Player player){
		player.openInventory(_inv);
	}
	
	public Inventory getInventory(){
		return _inv;
	}
	
}
