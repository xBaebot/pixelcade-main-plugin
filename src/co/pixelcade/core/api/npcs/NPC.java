package co.pixelcade.core.api.npcs;

import java.util.ArrayList;

import net.minecraft.server.v1_8_R3.NBTTagCompound;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;

import co.pixelcade.core.Core;
import co.pixelcade.core.api.npcs.EntityHider.Policy;

public class NPC {

	public static ArrayList<NPC> npcs = new ArrayList<>();
	
	private String name;
	private String[] multilineName;
	private Location location;
	private EntityType type;
	private Entity entity;
	private ArmorStand nameEntity;
	private Slime nameSlime;

	public NPC(String name, Location location, EntityType type) {
		this.name = name;
		this.location = location;
		this.type = type;
		this.multilineName = null;
		npcs.add(this);
	}

	public void setMultiLine(String line1, String line2) {
		multilineName = new String[]{};
		multilineName[0] = line1;
		multilineName[1] = line2;
	}
	
	public void spawnNPC() {
		entity = (Entity) location.getWorld().spawnEntity(location, type);
		Location loc = new Location(location.getWorld(), location.getX(), location.getY() - 0.5, location.getZ());
		nameEntity = (ArmorStand) location.getWorld().spawn(location, ArmorStand.class);
		nameSlime = (Slime) location.getWorld().spawn(loc, Slime.class);
		nameSlime.setSize(-1);
		nameSlime.setPassenger(nameEntity);
		EntityHider hider = new EntityHider(Core.instance, Policy.BLACKLIST);
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			
			hider.hideEntity(player, (Entity) nameSlime);
			
		}
		
		nameEntity.setCustomName(name);
		nameEntity.setCustomNameVisible(true);
		nameEntity.setVisible(false);
		nameEntity.setGravity(false);
		Vegetate(entity);
		Vegetate(nameSlime);
	}

	public boolean Vegetate(Entity vegeEntity) {
		if (entity == null)
			return false;
		net.minecraft.server.v1_8_R3.Entity nmsEnt = ((CraftEntity) vegeEntity)
				.getHandle();
		NBTTagCompound tag = new NBTTagCompound();
		nmsEnt.c(tag);
		tag.setBoolean("NoAI", true);
		tag.setInt("Fire", 0);
		nmsEnt.f(tag);
		return true;
	}

}
