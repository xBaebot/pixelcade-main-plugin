package co.pixelcade.core.common;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public abstract class Module implements Listener{
	
	private String name;
	private boolean events;
	
	public Module(String name, boolean events){
		this.name = name;
		this.events = events;
	}
	
	public Module(String name){
		this(name, true);
	}
	
	public Module(){
		this("Default", true);
	}
	
	public void registerEvents(){
		Bukkit.getPluginManager().registerEvents(this, Bukkit.getPluginManager().getPlugin("Core"));
	}
	
	public String getModuleName(){
		return name;
	}
	
	
}
