package co.pixelcade.core.common;


import org.apache.commons.lang.StringUtils;

public class UtilString {
	
	public static String center(String text) {
		int length = text.length();
		int spaces = ((52-length) / 2);
		int i = 0;
		do {
			text = " " + text;
			i +=1;
		} while(i < spaces);
		return text;
	}
	
	public static String build(String[] array, int index) {
		String result = "";
		
		for(int i = index; i < array.length; i++) {
			result = result + array[i] + " ";
		}
		return result;
	}
	
	public static String format(String word) {
		return capitalise(word.toLowerCase().trim().replace('_', ' ').replace('-', ' '));
	}
	
	public static String capitalise(String word) {
		return StringUtils.capitaliseAllWords(word);
	}
}
