package co.pixelcade.core.common;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Message {

	public enum MessagePrefix{
		
		SERVER("Server", ChatColor.GREEN),
		ERROR("Error", ChatColor.RED),
		HIERARCHY("Hierarchy", ChatColor.GREEN),
		STATE("State", ChatColor.GREEN),
		BROADCAST("Broadcast", ChatColor.DARK_PURPLE),
		HUB("Hub", ChatColor.GREEN),
		GAME("Game", ChatColor.DARK_GREEN),
		ADVENTURE("Adventure", ChatColor.LIGHT_PURPLE);
		
		private String name;
		private ChatColor color;
		
		private MessagePrefix(String name, ChatColor color){
			this.name = name;
			this.color = color;
		}
		
		public String getName(){
			return name;
		}
		
		public ChatColor getColor(){
			return color;
		}
		
	}
	
	public static void sendMessage(Player player, MessagePrefix prefix, String message){
		player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + ">> " + ChatColor.GRAY + message);
	}
	
	public static void broadcast(String message){
		Bukkit.broadcastMessage("�c�lBROADCAST >> �7" + message);
	}
	
	public static void log(String message){
		System.out.println(message);
	}
	
	public static String spaceOut(int space){
		if (space == 4) {
			return "    ";
		}
		
		return " ";
	}
}
