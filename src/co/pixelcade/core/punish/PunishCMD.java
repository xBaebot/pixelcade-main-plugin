package co.pixelcade.core.punish;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import co.pixelcade.core.client.Rank;
import co.pixelcade.core.commands.ServerCommand;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;
import co.pixelcade.core.common.UtilString;

public class PunishCMD extends ServerCommand {

	private static PunishCMD instance = new PunishCMD();
	
	public static PunishCMD getInstance() {
		return instance;
	}
	
	public PunishCMD() {
		super("punish", "<player> <reason>", Rank.JRMOD, 2);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute() {
		OfflinePlayer target = null;
		target = Bukkit.getServer().getOfflinePlayer(getArgs()[0]);
		if (target == null) {
			Message.sendMessage(getSender(), MessagePrefix.ERROR, "Invalid argument: Player does not exist!");
			return;
		}
		String reason = UtilString.build(getArgs(), 1);
		
		PunishUI.OpenUI(getSender(), target, reason);
	}

}
