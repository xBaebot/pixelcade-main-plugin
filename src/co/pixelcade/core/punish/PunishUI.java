package co.pixelcade.core.punish;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import co.pixelcade.core.Core;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.punish.Punishment.PunishmentType;
import co.pixelcade.core.util.UtilItem;
import co.pixelcade.core.util.UtilMath;
import co.pixelcade.core.util.UtilTime;
import co.pixelcade.core.util.UtilTime.TimeUnit;
import co.pixelcade.core.uuid.MojangUUID;

public class PunishUI implements org.bukkit.event.Listener {

	private static List<UUID> PunishOpen = new ArrayList<UUID>();

	public static void OpenUI(Player p, OfflinePlayer target, String reason) {
		Inventory inv = Bukkit.createInventory(null, 54,
				"Punish " + target.getName());
		AddItem(inv, ChatColor.RED + target.getName(), new String[] { reason },
				UtilItem.GetHead(target.getName()), 4);
		List<Punishment> puns = target.isOnline() ? Core.getClient(
				target.getPlayer()).getPunishments() : Punish
				.fetchPunishments(target);
		Punishment ActiveMute = Punish.getActiveMute(puns);
		Punishment ActiveBan = Punish.getActiveBan(puns);
		if (Core.getClient(p).getRank().isPermissible(Rank.MOD)) {
			if (ActiveBan == null) {
				AddItem(inv, ChatColor.GREEN + "No Active Ban", new String[0],
						new ItemStack(Material.BUCKET, 1), 12);
			} else {
				String time = "Permanent";
				String expires = "Never";
				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Timestamp(ActiveBan.TimeIssued));
				String punisher = ActiveBan.Punisher;
				if (punisher.startsWith("*UUID*-"))
					punisher = MojangUUID.getNameFromUUID(punisher.replace(
							"*UUID*-", ""));
				if (!ActiveBan.isPermanent()) {
					time = UtilTime.Convert(ActiveBan.Time);
					expires = UtilTime.Convert(ActiveBan.Expires
							- System.currentTimeMillis());
				}
				AddItem(inv, ChatColor.GREEN + "Unban Player", new String[] {
						"",
						ChatColor.BLUE + "" + ChatColor.UNDERLINE
								+ "Punishment Info:",
						"",
						ChatColor.YELLOW + "Time: " + ChatColor.WHITE + time,
						ChatColor.YELLOW + "Expires In: " + ChatColor.WHITE
								+ expires,
						ChatColor.YELLOW + "Issued On: " + ChatColor.WHITE
								+ date,
						"",
						ChatColor.YELLOW + "Issued By: " + ChatColor.WHITE
								+ punisher, ChatColor.YELLOW + "Reason: ",
						ChatColor.WHITE + ActiveBan.Reason }, new ItemStack(
						Material.LAVA_BUCKET, 1), 12);
			}
			if (ActiveMute == null) {
				AddItem(inv, ChatColor.GREEN + "No Active Mute", new String[0],
						new ItemStack(Material.BUCKET, 1), 14);
			} else {
				String time = "Permanent";
				String expires = "Never";
				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Timestamp(ActiveMute.TimeIssued));
				String punisher = ActiveMute.Punisher;
				if (punisher.startsWith("*UUID*-"))
					punisher = MojangUUID.getNameFromUUID(punisher.replace(
							"*UUID*-", ""));
				if (!ActiveMute.isPermanent()) {
					time = UtilTime.Convert(ActiveMute.Time);
					expires = UtilTime.Convert(ActiveMute.Expires
							- System.currentTimeMillis());
				}
				AddItem(inv, ChatColor.GREEN + "Unmute Player", new String[] {
						"",
						ChatColor.BLUE + "" + ChatColor.UNDERLINE
								+ "Punishment Info:",
						"",
						ChatColor.YELLOW + "Time: " + ChatColor.WHITE + time,
						ChatColor.YELLOW + "Expires In: " + ChatColor.WHITE
								+ expires,
						ChatColor.YELLOW + "Issued On: " + ChatColor.WHITE
								+ date,
						"",
						ChatColor.YELLOW + "Issued By: " + ChatColor.WHITE
								+ punisher, ChatColor.YELLOW + "Reason: ",
						ChatColor.WHITE + ActiveMute.Reason }, new ItemStack(
						Material.WATER_BUCKET, 1), 14);
			}
		}
		AddItem(inv, ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Warn",
				new String[0], new ItemStack(Material.PAPER, 1), 13);
		if (puns.size() > 0) {
			AddItem(inv, "View " + puns.size() + " past punishments...",
					new String[0], new ItemStack(Material.ARROW, 1), 22);
		} else {
			AddItem(inv, "No past punishments", new String[0], new ItemStack(
					Material.REDSTONE_BLOCK, 1), 22);
		}

		/*
		 * 
		 * Mute Buttons
		 */

		AddItem(inv, ChatColor.AQUA + "" + ChatColor.BOLD + "2 Hour Mute",
				new String[] {},
				new ItemStack(Material.INK_SACK, 1, (byte) 12), 28);
		AddItem(inv, ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "4 Hour Mute",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 6),
				29);
		AddItem(inv, ChatColor.BLUE + "" + ChatColor.BOLD + "12 Hour Mute",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 4),
				30);
		AddItem(inv, ChatColor.DARK_GREEN + "" + ChatColor.BOLD
				+ "24 Hour Mute", new String[] {}, new ItemStack(
				Material.INK_SACK, 1, (byte) 2), 31);
		AddItem(inv, ChatColor.YELLOW + "" + ChatColor.BOLD + "2 Day Mute",
				new String[] {},
				new ItemStack(Material.INK_SACK, 1, (byte) 11), 32);
		AddItem(inv, ChatColor.GOLD + "" + ChatColor.BOLD + "7 Day Mute",
				new String[] {},
				new ItemStack(Material.INK_SACK, 1, (byte) 14), 33);
		AddItem(inv, ChatColor.RED + "" + ChatColor.BOLD + "Permanent Mute",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 1),
				34);

		/*
		 * 
		 * Ban Buttons
		 */

		AddItem(inv, ChatColor.AQUA + "" + ChatColor.BOLD + "6 Hour Ban",
				new String[] {},
				new ItemStack(Material.INK_SACK, 1, (byte) 12), 46);
		AddItem(inv, ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "12 Hour Ban",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 6),
				47);
		AddItem(inv, ChatColor.BLUE + "" + ChatColor.BOLD + "24 Hour Ban",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 4),
				48);
		AddItem(inv, ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "2 Day Ban",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 2),
				49);
		AddItem(inv, ChatColor.YELLOW + "" + ChatColor.BOLD + "7 Day Ban",
				new String[] {},
				new ItemStack(Material.INK_SACK, 1, (byte) 11), 50);
		AddItem(inv, ChatColor.GOLD + "" + ChatColor.BOLD + "30 Day Ban",
				new String[] {},
				new ItemStack(Material.INK_SACK, 1, (byte) 14), 51);
		AddItem(inv, ChatColor.RED + "" + ChatColor.BOLD + "Permanent Ban",
				new String[] {}, new ItemStack(Material.INK_SACK, 1, (byte) 1),
				52);

		p.openInventory(inv);
		PunishOpen.add(p.getUniqueId());
	}

	private static void AddItem(Inventory inv, String name, String[] lore,
			ItemStack is, int slot) {
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		List<String> list = new ArrayList<String>();
		for (String s : lore) {
			list.add(ChatColor.RESET + s);
		}
		meta.setLore(list);
		is.setItemMeta(meta);
		inv.setItem(slot, is);
	}

	@EventHandler
	public void onInvClose(InventoryCloseEvent e) {
		if (e.getPlayer() instanceof Player) {
			Player p = (Player) e.getPlayer();
			if (PunishOpen.contains(p.getUniqueId())) {
				PunishOpen.remove(p.getUniqueId());
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if (!(e.getWhoClicked() instanceof Player))
			return;
		Player p = (Player) e.getWhoClicked();
		if (!(PunishOpen.contains(p.getUniqueId())))
			return;
		if (e.getCurrentItem().getItemMeta().getDisplayName() == null)
			return;
		e.setCancelled(true);
		if (e.getCurrentItem().getType().equals(Material.INK_SACK)) {
			PunishmentType type = PunishmentType.Ban;
			long time = -1;
			if (e.getSlot() > 27 && e.getSlot() < 35) {
				if (ChatColor.stripColor(
						e.getCurrentItem().getItemMeta().getDisplayName())
						.startsWith("Permanent")) {
					type = PunishmentType.Mute;
					time = -1;
				} else {
					type = PunishmentType.Mute;
					String[] words = ChatColor.stripColor(
							e.getCurrentItem().getItemMeta().getDisplayName())
							.split(" ");
					TimeUnit unit = TimeUnit.valueOf(words[1]);
					Double d = Double.valueOf(words[0]);
					time = d.longValue() * unit.getTimeInMillis();
				}
			} else if (e.getSlot() > 45 && e.getSlot() < 53) {
				if (ChatColor.stripColor(
						e.getCurrentItem().getItemMeta().getDisplayName())
						.startsWith("Permanent")) {
					type = PunishmentType.Ban;
					time = -1;
				} else {
					type = PunishmentType.Ban;
					String[] words = ChatColor.stripColor(
							e.getCurrentItem().getItemMeta().getDisplayName())
							.split(" ");
					TimeUnit unit = TimeUnit.valueOf(words[1]);
					Double d = Double.valueOf(words[0]);
					time = d.longValue() * unit.getTimeInMillis();
				}
			}
			String reason = e.getInventory().getItem(4).getItemMeta().getLore()
					.get(0);
			OfflinePlayer target = Bukkit.getOfflinePlayer(e.getInventory()
					.getTitle().replace("Punish ", ""));
			long timeissued = System.currentTimeMillis();
			Punish.addPunishment(target, p, reason, type, time, timeissued);
			String convertedTime = UtilTime.Convert(time);
			String cTime2 = UtilTime.Convert(time);
			if (time == -1) {
				convertedTime = "Permanent";
				cTime2 = "Never";
			}
			if (type.equals(PunishmentType.Ban)) {
				p.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD
						+ "You have been banned from this server!\n"
						+ ChatColor.WHITE + "You were banned for "
						+ ChatColor.GREEN + convertedTime + ChatColor.WHITE
						+ " by " + ChatColor.GREEN + p.getName()
						+ ChatColor.WHITE + ".\nYour ban expires in "
						+ ChatColor.GREEN + cTime2 + ChatColor.WHITE + ".\n"
						+ ChatColor.YELLOW + "You were banned because: "
						+ ChatColor.WHITE + reason + "\n"
						+ ChatColor.DARK_GREEN + "" + ChatColor.BOLD
						+ "You can appeal at " + ChatColor.GREEN
						+ Punish.AppealWebsite);
				for (Player item : Bukkit.getOnlinePlayers()) {
					if (Core.getClient(item).getRank()
							.isPermissible(Rank.JRMOD)) {
						item.sendMessage(ChatColor.RED
								+ "-----------------Punishment------------------");
						item.sendMessage(Core.getClient(p).getRank()
								.getPrefix()
								+ p.getName()
								+ ChatColor.GRAY
								+ " banned "
								+ ChatColor.BLUE
								+ target.getName()
								+ ChatColor.GRAY
								+ " for "
								+ ChatColor.GREEN
								+ convertedTime + ChatColor.GRAY + ".");
						item.sendMessage(ChatColor.AQUA + "Reason: "
								+ ChatColor.WHITE + reason);
					}
				}
			} else {
				for (Player item : Bukkit.getOnlinePlayers()) {
					if (Core.getClient(item).getRank()
							.isPermissible(Rank.JRMOD)
							|| item.getUniqueId().equals(target.getUniqueId())) {
						item.sendMessage(ChatColor.RED
								+ "-----------------Punishment------------------");
						item.sendMessage(Core.getClient(p).getRank()
								.getPrefix()
								+ p.getName()
								+ ChatColor.GRAY
								+ " muted "
								+ ChatColor.BLUE
								+ target.getName()
								+ ChatColor.GRAY
								+ " for "
								+ ChatColor.GREEN
								+ convertedTime + ChatColor.GRAY + ".");
						item.sendMessage(ChatColor.AQUA + "Reason: "
								+ ChatColor.WHITE + reason);
					}
				}
			}
			p.closeInventory();
			PunishOpen.remove(p.getUniqueId());
		} else if (e.getCurrentItem().getType().equals(Material.LAVA_BUCKET)) {
			String reason = e.getInventory().getItem(4).getItemMeta().getLore()
					.get(0);
			OfflinePlayer target = Bukkit.getOfflinePlayer(e.getInventory()
					.getTitle().replace("Punish ", ""));
			Punishment activeBan = Punish.getActiveBan(target);
			Punish.removePunishment(target, activeBan.ID, "*UUID*-"
					+ p.getUniqueId().toString(), reason,
					System.currentTimeMillis());
			for (Player item : Bukkit.getOnlinePlayers()) {
				if (Core.getClient(item).getRank().isPermissible(Rank.JRMOD)) {
					item.sendMessage(Core.getClient(p).getRank().getPrefix()
							+ p.getName() + ChatColor.GRAY + " unbanned "
							+ ChatColor.BLUE + target.getName()
							+ ChatColor.GRAY + ".");
					item.sendMessage(ChatColor.AQUA + "Reason: "
							+ ChatColor.WHITE + reason);
				}
			}
			PunishOpen.remove(p.getUniqueId());
			p.closeInventory();
		} else if (e.getCurrentItem().getType().equals(Material.WATER_BUCKET)) {
			String reason = e.getInventory().getItem(4).getItemMeta().getLore()
					.get(0);
			OfflinePlayer target = Bukkit.getOfflinePlayer(e.getInventory()
					.getTitle().replace("Punish ", ""));
			Punishment activeMute = Punish.getActiveMute(target);
			Punish.removePunishment(target, activeMute.ID, "*UUID*-"
					+ p.getUniqueId().toString(), reason,
					System.currentTimeMillis());
			for (Player item : Bukkit.getOnlinePlayers()) {
				if (Core.getClient(item).getRank().isPermissible(Rank.JRMOD)
						|| item.getUniqueId().equals(target.getUniqueId())) {
					item.sendMessage(Core.getClient(p).getRank().getPrefix()
							+ p.getName() + ChatColor.GRAY + " unmuted "
							+ ChatColor.BLUE + target.getName()
							+ ChatColor.GRAY + ".");
					item.sendMessage(ChatColor.AQUA + "Reason: "
							+ ChatColor.WHITE + reason);
				}
			}
			PunishOpen.remove(p.getUniqueId());
			p.closeInventory();
		} else if (e.getCurrentItem().getType().equals(Material.PAPER)) {
			if (e.getCurrentItem().getItemMeta().getDisplayName()
					.contains("Warn")) {
				String reason = e.getInventory().getItem(4).getItemMeta()
						.getLore().get(0);
				OfflinePlayer target = Bukkit.getOfflinePlayer(e.getInventory()
						.getTitle().replace("Punish ", ""));
				for (Player item : Bukkit.getOnlinePlayers()) {
					if (Core.getClient(item).getRank()
							.isPermissible(Rank.JRMOD)
							|| item.getUniqueId().equals(target.getUniqueId())) {
						item.sendMessage(ChatColor.RED
								+ "------------------Warning-------------------");
						item.sendMessage(Core.getClient(p).getRank()
								.getPrefix()
								+ p.getName()
								+ ChatColor.GRAY
								+ " warned "
								+ ChatColor.BLUE
								+ target.getName()
								+ ChatColor.GRAY + ".");
						item.sendMessage(ChatColor.AQUA + "Reason: "
								+ ChatColor.WHITE + reason);
					}
				}
				Punish.addPunishment(target, p, reason, PunishmentType.Warn, 0,
						System.currentTimeMillis());
				p.closeInventory();
				PunishOpen.remove(p.getUniqueId());
			}
		} else if (e.getCurrentItem().getType().equals(Material.ARROW)) {
			OfflinePlayer target = Bukkit.getOfflinePlayer(e.getInventory()
					.getTitle().replace("Punish ", ""));
			List<Punishment> listPuns = target.isOnline() ? Core.getClient(
					target.getPlayer()).getPunishments() : Punish
					.fetchPunishments(target);
			int[] IDArray = new int[listPuns.size()];
			for (int i = 0; i < listPuns.size(); i++) {
				IDArray[i] = listPuns.get(i).ID;
			}
			UtilMath.QuickSort(IDArray);
			int slot = 18;
			Punishment activeMute = Punish.getActiveMute(target);
			Punishment activeBan = Punish.getActiveBan(target);
			for (int i = IDArray.length - 1; i >= 0; i--) {
				Punishment pun = null;
				for (Punishment itr : listPuns) {
					if (itr.ID == IDArray[i]) {
						pun = itr;
						break;
					}
				}
				if (pun == null)
					continue;
				ItemStack item = new ItemStack(Material.ANVIL, 1);
				if (pun.getType() == PunishmentType.Mute)
					item = new ItemStack(Material.BOOK, 1);
				else if (pun.getType() == PunishmentType.Warn)
					item = new ItemStack(Material.PAPER, 1);
				boolean active = false;
				if (activeMute != null && activeBan != null) {
					if (pun.getType() == PunishmentType.Mute
							&& pun.ID == activeMute.ID) {
						active = true;
					} else if (pun.getType() == PunishmentType.Ban
							&& pun.ID == activeBan.ID) {
						active = true;
					}
				}
				String punisher = pun.Punisher;
				if (punisher.startsWith("*UUID*-"))
					punisher = MojangUUID.getNameFromUUID(punisher.replace(
							"*UUID*-", ""));
				Bukkit.broadcastMessage(punisher);
				String reason = pun.Reason;
				String time = "Never";
				if (!pun.isPermanent()) {
					time = UtilTime.Convert(pun.Expires
							- System.currentTimeMillis());
				}
				if (!active) {
					time = "Expired";
				}
				String fullTime = "Permanent";
				if (!pun.isPermanent()) {
					fullTime = UtilTime.Convert(pun.Time);
				}
				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Timestamp(pun.TimeIssued));
				List<String> lore = new ArrayList<String>();
				if (active && (!(pun.getType().equals(PunishmentType.Warn)))) {
					lore.add(ChatColor.GREEN + "" + ChatColor.BOLD + "ACTIVE");
				}
				lore.add("");
				lore.add(ChatColor.YELLOW + "Type: " + ChatColor.WHITE
						+ pun.getType().name());
				lore.add(ChatColor.YELLOW + "Issued By: " + ChatColor.WHITE
						+ punisher);
				lore.add(ChatColor.YELLOW + "Issued On: " + ChatColor.WHITE
						+ date);
				lore.add("");
				if (!(pun.getType().equals(PunishmentType.Warn))) {
					lore.add(ChatColor.YELLOW + "Time: " + ChatColor.WHITE
							+ fullTime);
					lore.add(ChatColor.YELLOW + "Expires In: "
							+ ChatColor.WHITE + time);
					lore.add("");
				}
				lore.add(ChatColor.YELLOW + "Reason: ");
				lore.add(reason);
				AddItem(e.getInventory(), ChatColor.RED + "" + ChatColor.BOLD
						+ "Offense #" + (i + 1),
						lore.toArray(new String[lore.size()]), item, slot);
				slot++;
				if (slot == 27)
					break;
			}
			p.updateInventory();
		}
	}
}
