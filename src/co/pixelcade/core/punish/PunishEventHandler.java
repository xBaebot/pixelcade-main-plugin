package co.pixelcade.core.punish;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import co.pixelcade.core.commands.PlayerMuteableCommandResponseEvent;
import co.pixelcade.core.util.UtilTime;
import co.pixelcade.core.uuid.MojangUUID;

public class PunishEventHandler implements Listener {

	@EventHandler
	public void LoginResponse(PlayerLoginEvent e) {
		Player p = e.getPlayer();
		Punishment potentialBan = Punish.getActiveBan(p);
		if (potentialBan != null) {
			String reason = potentialBan.Reason;
			String time = "Never";
			if (!potentialBan.isPermanent()) {
				time = UtilTime.Convert(potentialBan.Expires
						- System.currentTimeMillis());
			}
			String fullTime = "Permanent";
			if (!potentialBan.isPermanent()) {
				fullTime = UtilTime.Convert(potentialBan.Time);
			}
			String punisher = potentialBan.Punisher;
			if (punisher.startsWith("*UUID*-"))
				punisher = MojangUUID.getNameFromUUID(punisher.replace(
						"*UUID*-", ""));
			String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(new Timestamp(potentialBan.TimeIssued));
			e.disallow(Result.KICK_OTHER, ChatColor.RED + "" + ChatColor.BOLD
					+ "You are banned from this server!\n" + ChatColor.WHITE
					+ "You were banned for " + ChatColor.GREEN + fullTime
					+ ChatColor.WHITE + " by " + ChatColor.GREEN + punisher
					+ ChatColor.WHITE + ".\nYou were banned on "
					+ ChatColor.GREEN + date + ChatColor.WHITE
					+ ". Your ban expires in " + ChatColor.GREEN + time
					+ ChatColor.WHITE + ".\n" + ChatColor.YELLOW
					+ "You were banned because: " + ChatColor.WHITE + reason
					+ "\n" + ChatColor.DARK_GREEN + "" + ChatColor.BOLD
					+ "You can appeal at " + ChatColor.GREEN
					+ Punish.AppealWebsite);
		}
	}

	@EventHandler
	public void ChatResponse(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		Punishment potentialMute = Punish.getActiveMute(p);
		if (potentialMute != null) {
			String time = "This mute is permanent.";
			if (!potentialMute.isPermanent()) {
				time = "This will expire in "
						+ UtilTime.Convert(potentialMute.Expires
								- System.currentTimeMillis()) + ".";
			}
			p.sendMessage(ChatColor.RED + "You have been muted! " + time);
			PacketPlayOutChat packet = new PacketPlayOutChat(
					ChatSerializer
							.a("{text:\"Click here\",color:\"aqua\",clickEvent:{action:\"run_command\",value:\"mutedetails\"},hoverEvent:{action:\"show_text\",value:\"Click to view details.\"},extra:[{text:\" to view the details of your mute.\",color:red}]}"));
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void MuteableCommandResponse(PlayerMuteableCommandResponseEvent e) {
		Player p = e.getPlayer();
		Punishment potentialMute = Punish.getActiveMute(p);
		if (potentialMute != null) {
			String time = "This mute is permanent.";
			if (!potentialMute.isPermanent()) {
				time = "This will expire in "
						+ UtilTime.Convert(potentialMute.Expires
								- System.currentTimeMillis()) + ".";
			}
			p.sendMessage(ChatColor.RED + "You have been muted! " + time);
			PacketPlayOutChat packet = new PacketPlayOutChat(
					ChatSerializer
							.a("{text:\"Click here\",color:\"aqua\",clickEvent:{action:\"run_command\",value:\"mutedetails\"},hoverEvent:{action:\"show_text\",value:\"Click to view details.\"},extra:[{text:\" to view the details of your mute.\",color:red}]}"));
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().equalsIgnoreCase("mutedetails")) {
			Player p = e.getPlayer();
			Punishment potentialMute = Punish.getActiveMute(p);
			if (potentialMute != null) {
				String time = "Never";
				if (!potentialMute.isPermanent()) {
					time = UtilTime.Convert(potentialMute.Expires
							- System.currentTimeMillis());
				}
				String punisher = potentialMute.Punisher;
				if (punisher.startsWith("*UUID*-"))
					punisher = MojangUUID.getNameFromUUID(punisher.replace(
							"*UUID*-", ""));
				String fullTime = "Permanent";
				if (!potentialMute.isPermanent()) {
					fullTime = UtilTime.Convert(potentialMute.Time);
				}
				String reason = potentialMute.Reason;
				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Timestamp(potentialMute.TimeIssued));
				p.sendMessage(new String[] {
						ChatColor.YELLOW + "Time: " + ChatColor.WHITE
								+ fullTime,
						ChatColor.YELLOW + "Expires In: " + ChatColor.WHITE
								+ time,
						ChatColor.YELLOW + "Issued By: " + ChatColor.WHITE
								+ punisher,
						ChatColor.YELLOW + "Issued On: " + ChatColor.WHITE
								+ date,
						ChatColor.YELLOW + "Reason: " + ChatColor.WHITE
								+ reason });
				e.setCancelled(true);
			}
		}
	}

}
