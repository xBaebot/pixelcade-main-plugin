package co.pixelcade.core.punish;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import co.pixelcade.core.Core;
import co.pixelcade.core.client.DataStorage;
import co.pixelcade.core.punish.Punishment.PunishmentType;
import co.pixelcade.core.punish.Punishment.RemovedPunishment;

public class Punish {

	private static JavaPlugin pl;

	public static final String AppealWebsite = "www.pixelcade.co.uk";

	public static void Enable(JavaPlugin plugin) {
		String SQLPunsDB = "CREATE TABLE IF NOT EXISTS `Punishments` ("
				+ "`id` INT NOT NULL AUTO_INCREMENT, "
				+ "`uuid` varchar(65) NOT NULL, " + "`time` BIGINT, "
				+ "`timeIssued` BIGINT, " + "`reason` TEXT, "
				+ "`punisher` TEXT, " + "`metaData` TEXT, "
				+ "PRIMARY KEY (`id`)" + ");";
		DataStorage.getInstance().runUpdateStatement(SQLPunsDB);
		String SQLRemDB = "CREATE TABLE IF NOT EXISTS `RemovedPunishments` ("
				+ "`id` INT NOT NULL, " + "`removedBy` TEXT, "
				+ "`time` BIGINT, " + "`reason` TEXT, " + ");";
		DataStorage.getInstance().runUpdateStatement(SQLRemDB);
		pl = plugin;
		Bukkit.getPluginManager().registerEvents(new PunishEventHandler(), pl);
		Bukkit.getPluginManager().registerEvents(new PunishUI(), pl);
	}

	public static void removePunishment(OfflinePlayer p, int punishmentID,
			String removedBy, String reason, long time) {
		DataStorage.getInstance().runUpdateStatement(
				"INSERT INTO `RemovedPunishments`(`id`,`removedBy`,`time`,`reason`) VALUES('"
						+ punishmentID + "','" + removedBy + "','" + time
						+ "','" + reason + "');");
		if (p.isOnline()) {
			Core.getClient(p.getPlayer()).recalibratePunishments();
		}
	}

	public static void addPunishment(OfflinePlayer target,
			OfflinePlayer punisher, String reason, PunishmentType type,
			long time, long timeIssued) {
		Punishment p = Punishment.CreateNew(target, time, timeIssued, reason,
				"*UUID*-" + punisher.getUniqueId().toString(), type);
		addPunishment(p);
	}

	public static void addPunishment(Punishment p) {
		String metaD = "";
		for (int i = 0; i < p.MetaData.size(); i++) {
			String str = p.MetaData.get(i).toString() + ":::";
			if (p.MetaData.get(i) instanceof PunishmentType)
				str = "*Type*-" + ((PunishmentType) p.MetaData.get(i)).name();
			metaD += str;
		}
		metaD.substring(0, metaD.length() - 3);
		DataStorage
				.getInstance()
				.runUpdateStatement(
						"INSERT INTO `Punishments` (`uuid`,`time`,`timeIssued`,`reason`,`punisher`,`metaData`) VALUES('"
								+ p.Target.getUniqueId().toString()
								+ "','"
								+ p.Time
								+ "','"
								+ p.TimeIssued
								+ "','"
								+ p.Reason
								+ "','"
								+ p.Punisher
								+ "','"
								+ metaD
								+ "');");
		if (p.Target.isOnline()) {
			Core.getClient(p.Target.getPlayer()).recalibratePunishments();
		}
	}

	public static Punishment getMostRecentMute(List<Punishment> puns) {
		int num = 0;
		for (int i = 0; i < puns.size(); i++) {
			if (puns.get(i).getType().equals(PunishmentType.Mute)) {
				num++;
			}
		}
		long[] times = new long[num];
		if (times.length == 0)
			return null;
		int ttt = 0;
		for (int i = 0; i < puns.size(); i++) {
			if (puns.get(i).getType().equals(PunishmentType.Mute)) {
				times[ttt] = puns.get(i).TimeIssued;
				ttt++;
			}
		}
		int indx = 0;
		for (int i = 0; i < times.length; i++) {
			if (times[i] > times[indx]) {
				indx = i;
			}
		}
		for (Punishment pun : puns) {
			if (pun.Time == times[indx])
				return pun;
		}
		return null;
	}

	public static Punishment getMostRecentBan(List<Punishment> puns) {
		int num = 0;
		for (int i = 0; i < puns.size(); i++) {
			if (puns.get(i).getType().equals(PunishmentType.Ban)) {
				num++;
			}
		}
		long[] times = new long[num];
		if (times.length == 0)
			return null;
		int ttt = 0;
		for (int i = 0; i < puns.size(); i++) {
			if (puns.get(i).getType().equals(PunishmentType.Ban)) {
				times[ttt] = puns.get(i).TimeIssued;
				ttt++;
			}
		}
		int indx = 0;
		for (int i = 0; i < times.length; i++) {
			if (times[i] > times[indx]) {
				indx = i;
			}
		}
		for (Punishment pun : puns) {
			if (pun.Time == times[indx])
				return pun;
		}
		return null;
	}

	public static Punishment getActiveMute(List<Punishment> puns) {
		for (Punishment pun : puns) {
			if (pun.getType() != null) {
				Punishment recent = getMostRecentMute(puns);
				if(recent == null)
					return recent;
				if (pun.getType() == PunishmentType.Mute && pun.isActive()
						&& pun.ID == recent.ID) {
					return pun;
				}
			}
		}
		return null;
	}

	public static Punishment getActiveBan(List<Punishment> puns) {
		for (Punishment pun : puns) {
			if (pun.getType() != null) {
				Punishment recent = getMostRecentBan(puns);
				if(recent == null)
					return recent;
				if (pun.getType() == PunishmentType.Ban && pun.isActive()
						&& pun.ID == recent.ID) {
					return pun;
				}
			}
		}
		return null;
	}

	public static Punishment getActiveMute(OfflinePlayer p) {
		if (p.isOnline()) {
			return getActiveMute(Core.getClient(p.getPlayer()).getPunishments());
		}
		return getActiveMute(fetchPunishments(p));
	}

	public static Punishment getActiveBan(OfflinePlayer p) {
		if (p.isOnline()) {
			return getActiveBan(Core.getClient(p.getPlayer()).getPunishments());
		}
		return getActiveBan(fetchPunishments(p));
	}

	public static List<Punishment> fetchPunishments(OfflinePlayer p) {
		List<Punishment> list = new ArrayList<Punishment>();
		try {
			PreparedStatement s1 = DataStorage
					.getInstance()
					.getConnection()
					.prepareStatement(
							"SELECT * FROM `Punishments` WHERE `uuid`='"
									+ p.getUniqueId().toString() + "';");
			ResultSet rs1 = s1.executeQuery();
			while (rs1.next()) {
				Punishment pun = new Punishment();
				pun.MetaData = new ArrayList<Object>();
				RemovedPunishment rp = pun.new RemovedPunishment();
				PreparedStatement s2 = DataStorage
						.getInstance()
						.getConnection()
						.prepareStatement(
								"SELECT * FROM `RemovedPunishments` WHERE `id`='"
										+ rs1.getInt("id") + "';");
				ResultSet rs2 = s2.executeQuery();
				if (rs2.next()) {
					rp.ID = rs2.getInt("id");
					rp.Reason = rs2.getString("reason");
					rp.Time = rs2.getLong("time");
					rp.RemovedBy = rs2.getString("removedBy");
					pun.MetaData.add(rp);
				}
				rs2.close();
				s2.close();
				pun.ID = rs1.getInt("id");
				pun.Time = rs1.getLong("time");
				pun.TimeIssued = rs1.getLong("timeIssued");
				pun.Expires = pun.TimeIssued + pun.Time;
				pun.Reason = rs1.getString("reason");
				pun.Target = p;
				pun.Punisher = rs1.getString("punisher");
				String[] data = rs1.getString("metaData").split(":::");
				for (String s : data) {
					if (s.startsWith("*Type*-")) {
						PunishmentType type = PunishmentType.valueOf(s.replace(
								"*Type*-", ""));
						pun.setType(type);
					}
				}
				list.add(pun);
			}
			s1.close();
			rs1.close();
		} catch (SQLException e) {
			Core.manageError(e);
		}
		return list;
	}

}
