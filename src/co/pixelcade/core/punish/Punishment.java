package co.pixelcade.core.punish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;

public class Punishment {

	public int ID;
	public OfflinePlayer Target;
	public long Time;
	public long TimeIssued;
	public long Expires;
	public String Reason;
	public String Punisher;
	public List<Object> MetaData;

	public RemovedPunishment RemovePunishment(String RemovedBy, String Reason,
			long Time) {
		RemovedPunishment rp = new RemovedPunishment();
		rp.ID = ID;
		rp.RemovedBy = RemovedBy;
		rp.Reason = Reason;
		rp.Time = Time;
		MetaData.add(rp);
		return rp;
	}

	public RemovedPunishment getRemoveData() {
		for (Object o : MetaData) {
			if (o instanceof RemovedPunishment)
				return (RemovedPunishment) o;
		}
		return null;
	}

	public void setType(PunishmentType Type) {
		MetaData.add(Type);
	}

	public PunishmentType getType() {
		for (Object o : MetaData) {
			if (o instanceof PunishmentType)
				return (PunishmentType) o;
		}
		return null;
	}

	public boolean isPermanent() {
		return Time == -1;
	}

	public boolean isActive() {
		boolean b = isRemoved();
		if (Expires == -1 && !b)
			return true;
		if (Expires - System.currentTimeMillis() > 0 && !b)
			return true;
		return false;
	}

	public boolean isRemoved() {
		return getRemoveData() != null;
	}

	public static Punishment CreateNew(int ID, OfflinePlayer Target, long Time,
			long TimeIssued, String Reason, String Punisher, PunishmentType Type) {
		Punishment p = new Punishment();
		p.ID = ID;
		p.Target = Target;
		p.Time = Time;
		p.TimeIssued = TimeIssued;
		p.Expires = Time == -1 ? -1 : TimeIssued + Time;
		p.Reason = Reason;
		p.Punisher = Punisher;
		p.MetaData = new ArrayList<Object>();
		p.setType(Type);
		return p;
	}

	public static Punishment CreateNew(OfflinePlayer Target, long Time,
			long TimeIssued, String Reason, String Punisher, PunishmentType Type) {
		Punishment p = new Punishment();
		p.Target = Target;
		p.Time = Time;
		p.TimeIssued = TimeIssued;
		p.Expires = Time == -1 ? -1 : TimeIssued + Time;
		p.Reason = Reason;
		p.Punisher = Punisher;
		p.MetaData = new ArrayList<Object>();
		p.setType(Type);
		return p;
	}

	public enum PunishmentType {
		Ban, Mute, Warn
	}

	public class RemovedPunishment {

		public int ID;
		public String RemovedBy;
		public String Reason;
		public long Time;

	}

}
