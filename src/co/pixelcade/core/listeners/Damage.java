package co.pixelcade.core.listeners;

import co.pixelcade.core.Core;
import co.pixelcade.core.ServerType;
import co.pixelcade.core.common.Module;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by alex on 09/08/2015.
 */
public class Damage extends Module {
    private static Damage ourInstance = new Damage();

    public static Damage getInstance() {
        return ourInstance;
    }

    private Damage() {
        super();
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
    	if (Core.instance.getConfig().getBoolean("pvp") == true)
    	{
    		event.setCancelled(false);
    		return;
    	}
        if(event.getCause() == EntityDamageEvent.DamageCause.VOID) {
            if (event.getEntity() instanceof Player) {
                event.setCancelled(false);
                if(Core.serverType == ServerType.HUBSERVER || Core.serverType == ServerType.TESTSERVER) {
                    Location location = new Location(Bukkit.getWorld("Hub"), -371.479, 67, -522.175);
                    event.getEntity().teleport(location);
                }else{
                    event.getEntity().teleport(new Location(Bukkit.getWorld("GameLobby"), -372.499, 8, -520.119));
                }
                event.setDamage(0);
            }
        }
        if (!(event.getEntity() instanceof Player) && event.getCause() == EntityDamageEvent.DamageCause.VOID)
        {
            event.setCancelled(false);
        }
        if(Core.serverType == ServerType.HUBSERVER || Core.serverType == ServerType.TESTSERVER) {
            event.setCancelled(true);
        }
    }

}
