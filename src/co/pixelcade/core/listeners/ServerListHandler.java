package co.pixelcade.core.listeners;

import java.util.Random;

import co.pixelcade.core.common.Module;

import org.bukkit.event.EventHandler;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListHandler extends Module {
	
	private static ServerListHandler instance = new ServerListHandler();
	
	public static ServerListHandler getInstance(){
		return instance;
	}

	
	public ServerListHandler(){
		super("Event");
	}
	
	@EventHandler
	public void onList(ServerListPingEvent event){
		String prefix = "PixelCade: ";
		
		String message = "MISSINGNO";
		
		switch(new Random().nextInt(3)){
		case 1:
			message = "Come play some Minigames!";
			break;
		case 2:
			message = "Wild Server appears! ... Flee!";
			break;
		case 3:
			message = "Lol";
		}
		
		event.setMotd(prefix + message);
		
	}

}
