package co.pixelcade.core.listeners;

import java.util.ArrayList;

import co.pixelcade.core.common.Module;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockBreakHandler extends Module {
	
	public static ArrayList<Player> buildMode = new ArrayList<Player>();
	
	private static BlockBreakHandler instance = new BlockBreakHandler();
	
	public static BlockBreakHandler getInstance(){
		return instance;
	}
	
	public BlockBreakHandler(){
		super("Break");
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(e.getPlayer().getGameMode() == GameMode.CREATIVE){
			return;
		}
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(e.getPlayer().getGameMode() == GameMode.CREATIVE){
			return;
		}
		e.setCancelled(true);
	}

}
