package co.pixelcade.core.listeners;

import co.pixelcade.core.common.Module;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by alex on 09/08/2015.
 */
public class InventoryClick extends Module {


    private static InventoryClick ourInstance = new InventoryClick();

    public static InventoryClick getInstance() {
        return ourInstance;
    }

    private InventoryClick() {
        super();
    }

    @EventHandler
    public void onClick(InventoryClickEvent event){
            if(event.getClickedInventory() == event.getWhoClicked().getInventory()) {
                if (event.getWhoClicked().getGameMode() == GameMode.CREATIVE) {
                    event.setCancelled(false);
                    return;
                }
                event.setCancelled(true);
            }
    }

}
