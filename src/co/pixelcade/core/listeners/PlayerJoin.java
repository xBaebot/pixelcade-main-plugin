package co.pixelcade.core.listeners;

import co.pixelcade.core.Core;
import co.pixelcade.core.ServerType;
import co.pixelcade.core.api.Title;
import co.pixelcade.core.client.Client;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Module;
import co.pixelcade.core.menu.gamechooser.GameChooserMenu;
import co.pixelcade.core.menu.utils.UtilitiesMenu;
import co.pixelcade.core.nms.TabManager;
import co.pixelcade.core.tags.ServerScoreboard;
import co.pixelcade.core.uuid.MojangUUID;
import co.pixelcade.core.client.DataStorage;
import net.md_5.bungee.api.ChatColor;

import org.apache.logging.log4j.core.jmx.Server;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

public class PlayerJoin extends Module
{

	private Core coreInstance;

	private static PlayerJoin instance = new PlayerJoin(Core.instance);

	public static PlayerJoin getInstance()
	{
		return instance;
	}

	public PlayerJoin(Core coreInstance)
	{
		super("JoinListener");
		this.coreInstance = coreInstance;
	}

	public static List<ItemStack> joinItems = new ArrayList<ItemStack>();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();

		if (DataStorage.getInstance().containsElement("uuid", MojangUUID.getUUIDFromName(e.getPlayer().getName()), "ranks") == false)
		{
			DataStorage.getInstance().createRank(MojangUUID.getUUIDFromName(e.getPlayer().getName()));
		}
		Client c = new Client(e.getPlayer());
		Core.addClient(c);

		if (c.getRank().ordinal() >= Rank.ADMIN.ordinal())
		{
			p.setOp(true);
		}
		
		ServerScoreboard serverScoreboard = new ServerScoreboard(c);
		ServerScoreboard.addScoreboard(serverScoreboard, p);
		serverScoreboard.initScoreboard(p);

		for (Player player : Bukkit.getOnlinePlayers()) {
			ServerScoreboard.getScoreboard(player).refreshScoreboard(player);
		}
		
		new Title(e.getPlayer(), ChatColor.RED + "PixelCade", ChatColor.translateAlternateColorCodes(((String) "&").charAt(0), coreInstance.getConfig().getString("motd")));

		p.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=");
		p.sendMessage(" �aWelcome to PixelCade!");
		p.sendMessage(" �aYour rank is: " + ChatColor.AQUA + Core.getClient(p).getRank().getName());
		p.sendMessage(" ");
		p.sendMessage(" �aPurchase a rank at �bpixelcade.co.uk �aif you don't already have one!");
		p.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=�8-�9=");
		if (Core.serverType == ServerType.HUBSERVER)
		{
			e.getPlayer().setHealth(e.getPlayer().getMaxHealth());
			Location location = new Location(Bukkit.getWorld("Hub-Works"), 1.5, 20, 0.5);
			location.setYaw(180);
			e.getPlayer().teleport(location);
			e.getPlayer().setGameMode(GameMode.SURVIVAL);
			ItemStack selector = new ItemStack(Material.GOLDEN_APPLE);
			ItemMeta selectorMeta = selector.getItemMeta();
			selectorMeta.setDisplayName(org.bukkit.ChatColor.YELLOW + "PixelCade Games");
			selector.setItemMeta(selectorMeta);
			joinItems.add(selector);

			ItemStack toybox = new ItemStack(Material.ENDER_PORTAL_FRAME);
			ItemMeta toyboxMeta = toybox.getItemMeta();
			toyboxMeta.setDisplayName(org.bukkit.ChatColor.YELLOW + "Mythical Box");
			toybox.setItemMeta(toyboxMeta);
			joinItems.add(toybox);
			
			ItemStack utils = new ItemStack(Material.NETHER_STAR);
			ItemMeta utilsMeta = utils.getItemMeta();
			utilsMeta.setDisplayName("�eUtilities");
			utils.setItemMeta(utilsMeta);
			joinItems.add(utils);

			ItemStack playerMenu = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
			SkullMeta playerMeta = (SkullMeta) playerMenu.getItemMeta();
			playerMeta.setOwner(e.getPlayer().getName());
			playerMeta.setDisplayName(org.bukkit.ChatColor.YELLOW + "Player Settings" + org.bukkit.ChatColor.RED + " (COMING SOON)");
			playerMenu.setItemMeta(playerMeta);
			
			joinItems.add(playerMenu);

			ItemStack settings = new ItemStack(Material.MINECART);
			ItemMeta settingsMeta = settings.getItemMeta();
			settingsMeta.setDisplayName(ChatColor.YELLOW + "Lobby Rider" + ChatColor.RED + " (COMING SOON)");
			settings.setItemMeta(settingsMeta);
			joinItems.add(settings);

			Player player = e.getPlayer();
			player.getInventory().clear();
			player.getInventory().setItem(0, selector);
			player.getInventory().setItem(4, toybox);
			player.getInventory().setItem(8, playerMenu);
			player.getInventory().setItem(2, settings);
			player.getInventory().setItem(6, utils);

		} else
		{
			e.getPlayer().teleport(new Location(Bukkit.getWorld("MainLobby"), -19.176, 8, -9.528));
		}
		
		e.setJoinMessage("�9" + e.getPlayer().getDisplayName() + " �7joined.");
	}

	private void assignPerms(Client client)
	{

		List<String> playerPerms = new ArrayList<>();

		playerPerms.add("ultracosmetics.gadgets.paintballgun");
		playerPerms.add("ultracosmetics.gadgets.batblaster");
		playerPerms.add("ultracosmetics.gadgets.chickenator");
		playerPerms.add("ultracosmetics.gadgets.melonthrower");
		playerPerms.add("ultracosmetics.gadgets.etherealpearl");

		List<String> icarusPerms = new ArrayList<>();

		icarusPerms.add("ultracosmetics.gadgets.blizzardblaster");
		icarusPerms.add("ultracosmetics.gadgets.thorhammer");
		icarusPerms.add("ultracosmetics.gadgets.smashdown");
		icarusPerms.add("ultracosmetics.gadgets.rocket");
		icarusPerms.add("ultracosmetics.morph.creeper");

		List<String> diabloPerms = new ArrayList<>();

		diabloPerms.add("ultracosmetics.gadgets.discoball");
		diabloPerms.add("ultracosmetics.gadgets.colorbomb");
		diabloPerms.add("ultracosmetics.gadgets.fleshhook");
		diabloPerms.add("ultracosmetics.gadgets.explosivesheep");
		diabloPerms.add("ultracosmetics.gadgets.antigravity");
		diabloPerms.add("ultracosmetics.gadgets.tsunami");
		diabloPerms.add("ultracosmetics.gadgets.blackhole");

		if (client.getRank() == Rank.PLAYER)
		{

			PermissionAttachment attachment = client.getPlayer().addAttachment(Core.instance);

			for (String perm : playerPerms)
			{

				attachment.setPermission(perm, true);
			}
		}

		if (client.getRank() == Rank.ICARUS)
		{
			PermissionAttachment attachment = client.getPlayer().addAttachment(Core.instance);

			for (String perm : playerPerms)
			{

				attachment.setPermission(perm, true);
			}

			for (String perm : icarusPerms)
			{
				attachment.setPermission(perm, true);
			}

		}


	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLeave(PlayerQuitEvent e){
		Client c = Core.getClient(e.getPlayer());
		DataStorage.getInstance().setRank(c.getUuid(), c.getRank());
		Core.removeClient(e.getPlayer());
		e.setQuitMessage("�9" + e.getPlayer().getDisplayName() + " �7left.");
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		if (event.getPlayer().getItemInHand().getType().equals(Material.GOLDEN_APPLE))
		{
			event.setCancelled(true);
		}
		if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("Utilities") && event.getAction().toString().contains("RIGHT_CLICK"))
		{
			UtilitiesMenu menu = new UtilitiesMenu();
			menu.openInventory(event.getPlayer());
		}
		if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("PixelCade Games") && event.getAction().toString().contains("RIGHT_CLICK"))
		{
			GameChooserMenu menu = new GameChooserMenu();
			menu.openInventory(event.getPlayer());
		}
	}
	
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event)
	{
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onItemPickup(PlayerPickupItemEvent event)
	{
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
		{
			event.setCancelled(true);
		}
	}
	
}
