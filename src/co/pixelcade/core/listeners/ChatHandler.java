package co.pixelcade.core.listeners;

import co.pixelcade.core.common.Module;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.Core;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatHandler extends Module {
	
	private static ChatHandler instance = new ChatHandler();
	
	public static ChatHandler getInstance() {return instance;}
	
	public ChatHandler(){
		super("Chat", true);
	}
	
	@EventHandler
	public void setChatFormat(AsyncPlayerChatEvent e){
		Rank rank = Core.getClient(e.getPlayer()).getRank();
		if(rank == Rank.PLAYER){
			e.setFormat(ChatColor.DARK_GRAY + "%s�7:" + " " + ChatColor.GRAY + "%s");
			return;
		}
		e.setFormat("�8[" + rank.getColor() + "" + ChatColor.BOLD + rank.getName().toUpperCase() + "�8]" + " �8%s�7:" + " " + ChatColor.GRAY + "%s");
	}
	
}
