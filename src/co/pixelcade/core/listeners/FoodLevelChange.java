package co.pixelcade.core.listeners;

import co.pixelcade.core.common.Module;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelChange extends Module {


	public static FoodLevelChange instance = new FoodLevelChange();

	
	private FoodLevelChange(){
		super("Event");
	}
	@EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event)
    {
		event.setFoodLevel(20);
    }
}
