package co.pixelcade.core.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class UtilItem {

	public static ItemStack GetHead(String owner) {
		ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta sm = (SkullMeta) is.getItemMeta();
		sm.setOwner(owner);
		is.setItemMeta(sm);
		return is;
	}

}
