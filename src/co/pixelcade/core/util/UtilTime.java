package co.pixelcade.core.util;

public class UtilTime {

	public static String Convert(long time) {
		if (time < TimeUnit.Second.getTimeInMillis())
			return "A few moments...";
		String val = "";
		double deductVal = 0;
		if (time >= TimeUnit.Day.getTimeInMillis()) {
			double t = Math.floor(time / TimeUnit.Day.getTimeInMillis());
			if (t >= 1) {
				deductVal += t * TimeUnit.Day.getTimeInMillis();
				val += t + TimeUnit.Day.getChar() + " ";
			}
		}
		if (time >= TimeUnit.Hour.getTimeInMillis()) {
			double t = Math.floor(time - deductVal
					/ TimeUnit.Hour.getTimeInMillis());
			if (t >= 1) {
				deductVal += t * TimeUnit.Hour.getTimeInMillis();
				val += t + TimeUnit.Hour.getChar() + " ";
			}
		}
		if (time >= TimeUnit.Minute.getTimeInMillis()) {
			double t = Math.floor(time - deductVal
					/ TimeUnit.Minute.getTimeInMillis());
			if (t >= 1) {
				deductVal += t * TimeUnit.Minute.getTimeInMillis();
				val += t + TimeUnit.Minute.getChar() + " ";
			}
		}
		double t = Math.floor(time - deductVal
				/ TimeUnit.Second.getTimeInMillis());
		if (t >= 1) {
			val += t + TimeUnit.Second.getChar() + " ";
		}
		return val.substring(0, val.length() - 1);
	}

	public enum TimeUnit {

		Second(1000, "s"), Minute(1000 * 60, "m"), Hour(1000 * 60 * 60, "h"), Day(
				1000 * 60 * 60 * 24, "d");

		private String u;
		private long time;

		private TimeUnit(long time, String u) {
			this.u = u;
			this.time = time;
		}

		public String getChar() {
			return u;
		}

		public long getTimeInMillis() {
			return time;
		}

	}

}
