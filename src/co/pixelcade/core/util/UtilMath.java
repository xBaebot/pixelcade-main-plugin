package co.pixelcade.core.util;

public class UtilMath {

	public static boolean isSorted(int[] a) {
		for (int i = 1; i < a.length - 1; i++) {
			if (a[i] < a[i - 1])
				return false;
		}
		return true;
	}
	
	private static void Recurse_QuickSort(int[] a, int low, int hi){
		if(low < hi){
			int p = Partition_QuickSort(a, low, hi);
			Recurse_QuickSort(a, low, p);
			Recurse_QuickSort(a, p+1, hi);
		}
	}
	
	private static int Partition_QuickSort(int[] a, int low, int hi) {
		int i = low;
		int j = hi - 1;
		int pivot = a[low];
		while (true){
			do{
				j -= 1;
			}while(a[j] > pivot);
			do{
				i += 1;
			}while(a[i] < pivot);
			if(i < j){
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}else{
				return j;
			}
		}
	}

	public static void QuickSort(int[] a){
		if(isSorted(a))
			return;
		Recurse_QuickSort(a, 0, a.length - 1);
	}
	
}
