package co.pixelcade.core.client;

import org.bukkit.ChatColor;

public enum Rank {

	PLAYER(ChatColor.GREEN, "Player"),
	ICARUS(ChatColor.GREEN, "Icarus"),
	DIABLO(ChatColor.LIGHT_PURPLE, "Diablo"),
	MASTER(ChatColor.GOLD, "Master"),
	ARTIST(ChatColor.AQUA, "Artist"),
	BUILDER(ChatColor.BLUE, "Builder"),
	YOUTUBER(ChatColor.RED, "YouTuber"),
	JRMOD(ChatColor.DARK_PURPLE, "Jr.Mod"),
	MOD(ChatColor.LIGHT_PURPLE, "Mod"),
	QA(ChatColor.LIGHT_PURPLE, "QA"),
	ADMIN(ChatColor.AQUA, "Admin"),
	DEV(ChatColor.DARK_AQUA, "Dev"),
	MANAGEMENT(ChatColor.DARK_GREEN, "Management"),
	OWNER(ChatColor.GREEN, "Owner");
	
	private String name;
	private ChatColor colour;
	
	Rank(ChatColor color, String name){
		this.colour = color;
		this.name = name;
	}
	
	public ChatColor getColor(){
		return colour;
	}
	
	public String getName(){
		return name;
	}
	
	public String getPrefix(){
		return colour + name.toUpperCase() + " ";
	}
	
	public boolean isPermissible(Rank comparison){
		return this.ordinal() >= comparison.ordinal();
	}
	
}
