package co.pixelcade.core.client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.pixelcade.core.Core;

public class DataStorage {
	// Static instance - need getter
	private static DataStorage instance = new DataStorage();

	public static DataStorage getInstance() {
		return instance;
	}

	private Connection connection;

	public DataStorage() {
		openConnection();
	}

	public void openConnection() {
		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://149.202.53.203/PixelCade?autoReconnect=true",
					"root", "Tigger2003");
		} catch (Exception ex) {
			Core.manageError(ex);
			;
		}
	}

	public void closeConnection() {
		try {
			if (!connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Rank getRank(String uuid) {
		String query = "SELECT * FROM `ranks` WHERE `uuid`='" + uuid + "'";
		checkConnected();
		try {
			ResultSet res = connection.prepareStatement(query).executeQuery();
			Rank rank = Rank.PLAYER;
			while (res.next()) {
				rank = Rank.valueOf(res.getString("rank"));
			}
			res.close();

			return rank;
		} catch (SQLException e) {
			e.printStackTrace();
			return Rank.PLAYER;
		}
	}

	public boolean containsElement(String field, String element, String table) {
		String query = "SELECT `" + field + "` FROM `" + table + "` WHERE `"
				+ field + "`='" + element + "'";
		try {
			ResultSet res = connection.prepareStatement(query).executeQuery();
			boolean temp = res.next();
			res.close();

			return temp;
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
	}

	public void setRank(String uuid, Rank r) {
		checkConnected();
		try {
			String query = "";
			if (containsElement("uuid", uuid, "ranks")) {
				query = "UPDATE `ranks` SET `uuid`='" + uuid + "',`rank`='"
						+ r.name() + "' WHERE `uuid`='" + uuid + "'";
			} else {
				query = "INSERT INTO `ranks`(`uuid`, `rank`) VALUES ('" + uuid
						+ "','" + r.name() + "')";
			}
			PreparedStatement p = connection.prepareStatement(query);
			p.execute();
			p.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createRank(String uuid) {
		checkConnected();
		try {
			String query = "INSERT INTO `ranks`(`uuid`, `rank`) VALUES ('"
					+ uuid + "','" + "PLAYER" + "')";
			PreparedStatement p = connection.prepareStatement(query);
			p.execute();
			p.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void checkConnected() {
		try {
			if (connection.isClosed()) {
				openConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

		}
	}

	public Connection getConnection() {
		return connection;
	}

	public int runUpdateStatement(String sql) {
		try {
			PreparedStatement s = connection.prepareStatement(sql);
			int num = s.executeUpdate();
			s.close();
			return num;
		} catch (SQLException e) {
			Core.manageError(e);
		}
		return -1;
	}

}
