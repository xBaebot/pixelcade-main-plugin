package co.pixelcade.core.client;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import co.pixelcade.core.common.Message;
import co.pixelcade.core.punish.Punish;
import co.pixelcade.core.punish.Punishment;
import co.pixelcade.core.tags.ServerScoreboard;
import co.pixelcade.core.uuid.MojangUUID;

public class Client {

	private Player player;
	private String uuid;
	private Rank rank;
	private int shards;
	private int dubloons;
	private List<Punishment> punishments;

	public Client(Player player) {
		this.player = player;
		this.uuid = MojangUUID.getUUIDFromName(player.getDisplayName());
		this.rank = DataStorage.getInstance().getRank(uuid);
		this.shards = 200;
		this.dubloons = 500;
		this.punishments = Punish.fetchPunishments(player);
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Rank getRank() {
		return this.rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
		ServerScoreboard.getScoreboard(player).refreshScoreboard(player);
	}

	public int getShards() {
		return shards;
	}

	public void setShards(int shards) {
		this.shards = shards;
	}

	public int getDubloons() {
		return dubloons;
	}

	public void setDubloons(int dubloons) {
		this.dubloons = dubloons;
	}

	public Player getPlayer() {
		return this.player;
	}

	public List<Punishment> getPunishments() {
		return punishments;
	}

	public void addPunishment(Punishment p) {
		punishments.add(p);
	}

	public void recalibratePunishments() {
		punishments = Punish.fetchPunishments(player);
	}

	public void unlockMaster() {
		Message.broadcast(player.getDisplayName() + " has unlocked �6�lMASTER RANK�7.");
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.playSound(player.getLocation(), Sound.ENDERDRAGON_DEATH, 5, 5);
		}
	}
	
}
