package co.pixelcade.core.tags;

import co.pixelcade.core.Core;
import co.pixelcade.core.client.Client;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;

/**
 * Created by alex on 08/09/2015.
 */
public class ServerScoreboard
{

	private static HashMap<Player, ServerScoreboard> scoreboards = new HashMap<>();

	public static void addScoreboard(ServerScoreboard scoreboard, Player player)
	{
		scoreboards.put(player, scoreboard);
	}

	public static ServerScoreboard getScoreboard(Player player)
	{
		return scoreboards.get(player);
	}

	private Client client;
	private ScoreboardManager manager;
	private Scoreboard board;

	public ServerScoreboard(Client client)
	{
		this.client = client;
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
	}

	public void initScoreboard(Player player)
	{
		Objective objective = board.registerNewObjective(player.getDisplayName(), "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.GRAY + "" + ChatColor.BOLD + ">> PIXELCADE <<");

		Objective tagObjective = board.registerNewObjective(player.getDisplayName() + "-tags", "dummy");
		tagObjective.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		for (Player players : Bukkit.getOnlinePlayers())
		{
			Client client = Core.getClient(players);
			Team team = board.registerNewTeam(players.getDisplayName());
			team.setPrefix(client.getRank().getColor() + "" + ChatColor.BOLD + client.getRank().getName() + " �7");
			team.addPlayer(players);
		}
		
		objective.getScore("�1 ").setScore(30);
		objective.getScore("�a�lRank").setScore(29);
		objective.getScore(client.getRank().getName()).setScore(28);
		objective.getScore("�2 ").setScore(27);
		objective.getScore("�a�lCoins").setScore(26);
		objective.getScore("Coming Soon").setScore(25);
		objective.getScore("�3 ").setScore(24);
		objective.getScore("�a�lPixels").setScore(23);
		objective.getScore("Also Coming Soon").setScore(22);

		player.setScoreboard(board);
	}

	public void refreshScoreboard(Player player)
	{
		for (Objective obj : board.getObjectives())
		{
			obj.unregister();
		}

		Objective tagObjective = board.registerNewObjective(player.getDisplayName() + "-tags", "dummy");
		tagObjective.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		for (Player players : Bukkit.getOnlinePlayers())
		{
			Client client = Core.getClient(players);
			Team team = board.registerNewTeam(players.getDisplayName());
			team.setPrefix(client.getRank().getColor() + client.getRank().getName() + " �f");
			team.addPlayer(players);
		}
		
		Objective objective = board.registerNewObjective(player.getDisplayName(), "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.GRAY + "" + ChatColor.BOLD + ">> PIXELCADE <<");

		objective.getScore("�1 ").setScore(30);
		objective.getScore("�a�lRank").setScore(29);
		objective.getScore(client.getRank().getName()).setScore(28);
		objective.getScore("�2 ").setScore(27);
		objective.getScore("�a�lCoins").setScore(26);
		objective.getScore("Coming Soon").setScore(25);
		objective.getScore("�3 ").setScore(24);
		objective.getScore("�a�lPixels").setScore(23);
		objective.getScore("Also `Coming Soon").setScore(22);

		player.setScoreboard(board);
	}

}
