package co.pixelcade.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;

public class flyCommand extends ServerCommand{
	
	private static flyCommand instance = new flyCommand();
	
	public static flyCommand getInstance(){
		return instance;
	}
	
	public flyCommand(){
		super("fly", "(player)", Rank.ADMIN, 0);
	}

	@Override
	public void execute() {
		if(getArgs().length == 1){
			Player target = Bukkit.getPlayer(getArgs()[0]);
			Player sender = getSender();
			if(target == null){
				Message.sendMessage(getSender(), MessagePrefix.ERROR, ChatColor.RED + "target is null");
				return;
			}
			if(target.getAllowFlight() == true){
				target.setAllowFlight(false);
				target.setFlying(false);
				Message.sendMessage(getSender(), MessagePrefix.STATE, "Flying has been " + ChatColor.BOLD + "" + ChatColor.RED + "DISABLED" + ChatColor.DARK_AQUA + " for " + target);
			}else{
				target.setAllowFlight(true);
				target.setFlying(true);
				Message.sendMessage(getSender(), MessagePrefix.STATE, "Flying has been " + ChatColor.BOLD + "" + ChatColor.GREEN + "ENABLED" + ChatColor.DARK_AQUA + " for " + target);
			}
		}else{
			Player sender = getSender();
			if(sender.getAllowFlight()){
				sender.setAllowFlight(false);
				sender.setFlying(false);
				Message.sendMessage(getSender(), MessagePrefix.STATE, "Flying has been " + ChatColor.BOLD + "" + ChatColor.RED + "DISABLED" + "�3.");
			}else{
				sender.setAllowFlight(true);
				sender.setFlying(true);
				Message.sendMessage(getSender(), MessagePrefix.STATE, "Flying has been " + ChatColor.BOLD + "" + ChatColor.GREEN + "ENABLED" + "�3.");
			}
		}
	}

}
