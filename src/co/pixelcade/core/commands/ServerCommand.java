package co.pixelcade.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import co.pixelcade.core.Core;
import co.pixelcade.core.client.Client;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;
import co.pixelcade.core.common.Module;

public abstract class ServerCommand extends Module implements CommandExecutor {

	private String name;
	private String usage;
	private Rank rank;
	private Player sender;
	private Client cSender;
	private int length;
	private String[] args;
	private boolean disabled;

	public ServerCommand(String name, String usage, Rank rank, int length) {
		super("Command");
		this.name = name;
		this.usage = usage;
		this.rank = rank;
		this.length = length;
	}

	public abstract void execute();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		this.args = args;
		this.sender = (Player) sender;
		cSender = Core.getClient(this.sender);
		if (this instanceof Muteable) {
			PlayerMuteableCommandResponseEvent e = new PlayerMuteableCommandResponseEvent(
					cSender.getPlayer(), this);
			Bukkit.getServer().getPluginManager().callEvent(e);
			if (e.isCancelled()) {
				return true;
			}
		}
		if (args.length < this.length) {
			if (this.usage == null) {
				Message.sendMessage((Player) sender, MessagePrefix.SERVER,
						"Invalid arguments!");
			} else {
				Message.sendMessage(this.sender, MessagePrefix.SERVER,
						ChatColor.RED + "/" + this.name + " " + this.usage);
			}
			return true;
		}
		int requiredRank = this.rank.ordinal();
		int playerRank = cSender.getRank().ordinal();
		if (requiredRank > playerRank) {
			Message.sendMessage(
					this.sender,
					MessagePrefix.HIERARCHY,
					"You need rank " + this.rank.getColor()
							+ this.rank.getName() + " �7to use that command!");
			return true;
		}
		execute();
		return true;
	}

	public void registerCommand() {
		Bukkit.getPluginCommand(this.name).setExecutor(this);
	}

	public String[] getArgs() {
		return this.args;
	}

	public Player getSender() {
		return this.sender;
	}

}
