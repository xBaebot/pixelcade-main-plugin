package co.pixelcade.core.commands;

import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class gamemodeCommand extends ServerCommand{
	
	private static gamemodeCommand instance = new gamemodeCommand();
	
	public static gamemodeCommand getInstance(){
		return instance;
	}
	
	public gamemodeCommand(){
		super("gamemode", "<0,1,2,3,s,c,a>", Rank.ADMIN, 1);
	}
	
	public void execute(){
			Player p = getSender();
			String s = getArgs()[0];
			if(s.contains("0") || s.contains("s")){
				p.setGameMode(GameMode.SURVIVAL);
				Message.sendMessage(p, MessagePrefix.STATE, "Your gamemode was updated!");
			}else if(s.contains("1") || s.contains("c")){
				p.setGameMode(GameMode.CREATIVE);
				Message.sendMessage(p, MessagePrefix.STATE, "Your gamemode was updated!");
			}else if(s.contains("2") || s.contains("a")){
				p.setGameMode(GameMode.ADVENTURE);
				Message.sendMessage(p, MessagePrefix.STATE, "Your gamemode was updated!");
			}else if(s.contains("3")){
				p.setGameMode(GameMode.SPECTATOR);
				Message.sendMessage(p, MessagePrefix.STATE, "Your gamemode was updated!");
}
	}

}
