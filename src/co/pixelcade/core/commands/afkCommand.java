package co.pixelcade.core.commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import co.pixelcade.core.Core;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;

public class afkCommand extends ServerCommand{
	
	private static afkCommand instance = new afkCommand();
	
	public static afkCommand getInstance(){
		return instance;
	}
	
	private static ArrayList<Player> _afk = new ArrayList<>();
	
	public afkCommand(){
		super("afk", "", Rank.PLAYER, 0);
	}
	
	public void execute(){
		if (Core.getClient(getSender()).getRank().ordinal() > Rank.JRMOD.ordinal()) {
			Message.sendMessage(getSender(), MessagePrefix.ERROR, "Staff cannot go AFK!");
			return;
		}
		
		if(_afk.contains(getSender())){
			_afk.remove(getSender());
			Message.sendMessage(getSender(), MessagePrefix.STATE, "You are now AFK");
		}else{
			_afk.add(getSender());
			Message.sendMessage(getSender(), MessagePrefix.STATE, "You are no longer AFK");
		}
	}

}
