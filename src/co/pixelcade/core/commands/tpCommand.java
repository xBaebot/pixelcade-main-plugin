package co.pixelcade.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;

public class tpCommand extends ServerCommand{
	
	private static tpCommand instance = new tpCommand();
	
	public static tpCommand getInstance(){
		return instance;
	}
	
	public tpCommand(){
		super("tp", "(here) <player>", Rank.MOD, 1);
	}
	
	public void execute(){
		if(getArgs()[0].equalsIgnoreCase("here")){
			Player target = Bukkit.getPlayer(getArgs()[1]);
			Player admin = getSender();
			target.teleport(admin.getLocation());
			Message.sendMessage(admin, MessagePrefix.SERVER, "You teleported " + target.getDisplayName() + " to yourself!");
			Message.sendMessage(target, MessagePrefix.SERVER, admin.getDisplayName() + " teleported you to themself.");
			return;
		}
		Player target = Bukkit.getPlayer(getArgs()[0]);
		Player admin = getSender();
		admin.teleport(target.getLocation());
		Message.sendMessage(admin, MessagePrefix.SERVER, "You teleported to " + target.getDisplayName());
	}

}
