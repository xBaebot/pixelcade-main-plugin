package co.pixelcade.core.commands;

import co.pixelcade.core.client.Rank;
import co.pixelcade.game.Engine;

public class gmCommand extends ServerCommand {

	private static gmCommand instance = new gmCommand();

	public static gmCommand getInstance() {
		return instance;
	}

	public gmCommand(){
		super("gm", "", Rank.DEV, 1);
	}

	public void execute(){
		if (getArgs()[0].equals("begin")){
			Engine.getInstance().countdown();
		}
		if (getArgs()[0].equals("stop"))
		{
			Engine.getInstance().getEngine().unregister();
		}
	}
}