package co.pixelcade.core.commands;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerMuteableCommandResponseEvent extends Event implements
		Cancellable {

	private ServerCommand cmd;
	private Player p;

	public PlayerMuteableCommandResponseEvent(Player p, ServerCommand cmd) {
		this.cmd = cmd;
		this.p = p;
	}

	public Player getPlayer() {
		return p;
	}

	public ServerCommand getCommand() {
		return cmd;
	}

	/**
	 * HandlerList stuff
	 */
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private boolean b = false;

	@Override
	public boolean isCancelled() {
		return b;
	}

	@Override
	public void setCancelled(boolean arg0) {
		this.b = arg0;
	}
}
