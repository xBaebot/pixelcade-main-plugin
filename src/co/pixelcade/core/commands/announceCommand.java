package co.pixelcade.core.commands;

import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Created by alex on 15/08/2015.
 */
public class announceCommand extends ServerCommand {

	private static announceCommand instance = new announceCommand();

	public static announceCommand getInstance() {
		return instance;
	}

	public announceCommand(){
		super("announce", "<message>", Rank.MOD, 1);
	}

	public void execute(){
		StringBuilder sb = new StringBuilder();
		for(String s : getArgs()){
			if(s == getArgs()[0]){
				sb.append(s);
			}else{
				sb.append(" " + s);
			}
		}
		Message.broadcast(sb.toString());
	}

}
