package co.pixelcade.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import co.pixelcade.core.Core;
import co.pixelcade.core.ServerType;
import co.pixelcade.core.client.Rank;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;
import net.md_5.bungee.api.ChatColor;

/**
 * Created by alex on 09/08/2015.
 */
public class settypeCommand extends ServerCommand {

    private static settypeCommand instance = new settypeCommand();

    public static settypeCommand getInstance() {
        return instance;
    }

    public settypeCommand(){
        super("settype", "<TESTSERVER, HUBSERVER, GAMESERVER>", Rank.MANAGEMENT, 1);
    }

    public void execute(){
        Core.instance.getConfig().set("server-path", getArgs()[0]);
        Core.serverType = ServerType.valueOf(getArgs()[0]);
        for (Player player : Bukkit.getOnlinePlayers())
        {
        	Message.sendMessage(player, MessagePrefix.STATE, "The server's state was updated to " + ChatColor.YELLOW
        			+ Core.serverType.toString());
        }
        Core.instance.saveConfig();
        Bukkit.broadcastMessage("Server Restarting - State was Updated");
        Bukkit.getServer().shutdown();
    }

}
