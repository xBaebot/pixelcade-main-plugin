package co.pixelcade.core.commands;

import co.pixelcade.core.client.DataStorage;
import co.pixelcade.core.client.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import co.pixelcade.core.Core;
import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;
import co.pixelcade.core.uuid.MojangUUID;

public class setrankCommand extends ServerCommand{
	
	private static setrankCommand instance = new setrankCommand();
	
	public static setrankCommand getInstance(){
		return instance;
	}
	
	public setrankCommand(){
		super("setrank", "<player> <rank>", Rank.ADMIN, 2);
	}

	@Override
	public void execute() {
		Player player = Bukkit.getPlayer(getArgs()[0]);
		Rank rank = Rank.valueOf(getArgs()[1].toUpperCase());
		
		if (rank == Rank.MASTER) {
			Core.getClient(player).unlockMaster();
		}
		
		try{
			Core.getClient(Bukkit.getPlayer(getArgs()[0])).setRank(rank);
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		if (Core.getClient(Bukkit.getPlayer(getArgs()[0])).getRank() == rank)
		{
			
			if (rank != Rank.MASTER) {
			for (Player p : Bukkit.getOnlinePlayers())
			{
				
				Message.sendMessage(p, MessagePrefix.HIERARCHY, player.getDisplayName() + "'s rank was updated to " + rank.getColor() + rank.getName() + "�7.");
			}
			}
		}
		
	}

}
