package co.pixelcade.core;

/**
 * Created by aleca on 09/08/2015.
 */
public enum ServerType {

    TESTSERVER("Test"), HUBSERVER("Hub"), GAMESERVER("Game");

	String server;
	
	ServerType(String server) {
		this.server = server;
	}
	
	public String getServer() {
		return this.server;
	}
}
