package co.pixelcade.core.events;

/**
 * Created by alex on 24/08/2015.
 */
public enum Event
{

	NONE("None"), PARTY("Party");

	private String _name;

	private Event(String name)
	{
		this._name = name;
	}

	public String getName()
	{
		return this._name;
	}

	private static Event currentEvent = Event.NONE;

	public static Event getCurrentEvent()
	{
		return currentEvent;
	}

	public static void setCurrentEvent(Event event)
	{
		currentEvent = event;
	}

}
