package co.pixelcade.core.events;

import co.pixelcade.core.Core;
import co.pixelcade.core.client.Rank;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

/**
 * Created by alex on 24/08/2015.
 */
public class EventParty
{

	public static void start()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			if(Core.getClient(player).getRank().ordinal() >= Rank.ADMIN.ordinal())
			{
				player.setGameMode(GameMode.CREATIVE);
			}
		}
	}

	public static void stop()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			player.setGameMode(GameMode.SURVIVAL);
		}
	}

}
