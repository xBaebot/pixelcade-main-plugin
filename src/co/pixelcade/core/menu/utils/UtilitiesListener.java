package co.pixelcade.core.menu.utils;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import co.pixelcade.core.common.Module;

public class UtilitiesListener extends Module {

	private Player player;
	
	public UtilitiesListener(Player player)
	{
		this.player = player;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getClickedInventory().getName() == "Utilities Menu" && event.getWhoClicked() == player)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event)
	{
		if (event.getInventory().getName().contains("Utilities Menu"))
		{
			HandlerList.unregisterAll(this);
		}
	}
	
}
