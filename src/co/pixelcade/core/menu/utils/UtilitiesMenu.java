package co.pixelcade.core.menu.utils;

import java.util.Arrays;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import co.pixelcade.core.api.Menu;

/**
 * Created by alex on 09/08/2015.
 */
public class UtilitiesMenu extends Menu {

	public UtilitiesMenu(){
        super("Utilities Menu", 27);
        
        addItem("�aTest", 11, Material.YELLOW_FLOWER, Arrays.asList("�7This feature is coming soon!", "�7Please wait while we create it!"));
        addItem("�aTest", 13, Material.EMERALD, Arrays.asList("�7This feature is coming soon!", "�7Please wait while we create it!"));
        addItem("�aTest", 15, Material.RED_ROSE, Arrays.asList("�7This feature is coming soon!", "�7Please wait while we create it!"));
        addGlass(0, DyeColor.RED);
        addGlass(1, DyeColor.LIGHT_BLUE);
        addGlass(2, DyeColor.LIGHT_BLUE);
        addGlass(3, DyeColor.LIGHT_BLUE);
        addGlass(4, DyeColor.LIGHT_BLUE);
        addGlass(5, DyeColor.LIGHT_BLUE);
        addGlass(6, DyeColor.LIGHT_BLUE);
        addGlass(7, DyeColor.LIGHT_BLUE);
        addGlass(8, DyeColor.RED);
        addGlass(9, DyeColor.RED);
        addGlass(10, DyeColor.LIGHT_BLUE);
        addGlass(12, DyeColor.RED);
        addGlass(14, DyeColor.RED);
        addGlass(16, DyeColor.LIGHT_BLUE);
        addGlass(17, DyeColor.RED);
        addGlass(18, DyeColor.RED);
        addGlass(19, DyeColor.LIGHT_BLUE);
        addGlass(20, DyeColor.LIGHT_BLUE);
        addGlass(21, DyeColor.LIGHT_BLUE);
        addGlass(22, DyeColor.LIGHT_BLUE);
        addGlass(23, DyeColor.LIGHT_BLUE);
        addGlass(24, DyeColor.LIGHT_BLUE);
        addGlass(25, DyeColor.LIGHT_BLUE);
        addGlass(26, DyeColor.RED);
    }
    
	@Override
	public void openInventory(Player player)
	{
		new UtilitiesListener(player).registerEvents();
		super.openInventory(player);
	}
	
}
