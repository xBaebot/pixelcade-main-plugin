package co.pixelcade.core.menu.gamechooser;

import java.util.Arrays;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import co.pixelcade.core.api.Menu;

public class GameChooserMenu extends Menu {
	
	public GameChooserMenu()
	{
		super("PixelCade Games", 54);
		
		// Row 1
		addGlass(0, DyeColor.RED);
		addGlass(1, DyeColor.LIGHT_BLUE);
		addGlass(2, DyeColor.LIGHT_BLUE);
		addGlass(3, DyeColor.LIGHT_BLUE);
		addGlass(4, DyeColor.LIGHT_BLUE);
		addGlass(5, DyeColor.LIGHT_BLUE);
		addGlass(6, DyeColor.LIGHT_BLUE);
		addGlass(7, DyeColor.LIGHT_BLUE);
		addGlass(8, DyeColor.RED);
		
		// Row 2
		addGlass(9, DyeColor.RED);
		addGlass(10, DyeColor.LIGHT_BLUE);
		addItem("�aMinecraft Heroes", 11, Material.IRON_SWORD, Arrays.asList("�7Description to be added"));
		addItem("�fPixelating Soon!", 12, Material.WEB, Arrays.asList("�7This feature is coming soon!"));
		addGlass(13, DyeColor.RED);
		addItem("�fPixelating Soon!", 14, Material.WEB, Arrays.asList("�7This feature is coming soon!"));
		addItem("�fPixelating Soon!", 15, Material.WEB, Arrays.asList("�7This feature is coming soon!"));
		addGlass(16, DyeColor.LIGHT_BLUE);
		addGlass(17, DyeColor.RED);
		
		// Row 3
		addGlass(18, DyeColor.BLUE);
		addGlass(19, DyeColor.RED);
		addGlass(20, DyeColor.LIGHT_BLUE);
		addGlass(21, DyeColor.RED);
		addItem("�aReturn to the hub", 22, Material.BEACON, Arrays.asList("�7Click to go to the hub!"));
		addGlass(23, DyeColor.RED);
		addGlass(24, DyeColor.LIGHT_BLUE);
		addGlass(25, DyeColor.RED);
		addGlass(26, DyeColor.BLUE);
		
		// Row 4
		addGlass(27, DyeColor.BLUE);
		addGlass(28, DyeColor.RED);
		addGlass(29, DyeColor.LIGHT_BLUE);
		addItem("�fPixelating Soon!", 30, Material.WEB, Arrays.asList("�7This feature is coming soon!"));
		addGlass(31, DyeColor.RED);
		addItem("�fPixelating Soon!", 32, Material.WEB, Arrays.asList("�7This feature is coming soon!"));
		addGlass(33, DyeColor.LIGHT_BLUE);
		addGlass(34, DyeColor.RED);
		addGlass(35, DyeColor.BLUE);
		
		// Row 5
		addGlass(36, DyeColor.BLUE);
		addGlass(37, DyeColor.BLUE);
		addGlass(38, DyeColor.RED);
		addGlass(39, DyeColor.LIGHT_BLUE);
		addItem("�aUHC Brawl", 40, Material.GOLDEN_APPLE, Arrays.asList("�7Insert cool description here.", "�7...And here!"));
		addGlass(41, DyeColor.LIGHT_BLUE);
		addGlass(42, DyeColor.RED);
		addGlass(43, DyeColor.BLUE);
		addGlass(44, DyeColor.BLUE);
		
		// Row 6
		addGlass(45, DyeColor.RED);
		addGlass(46, DyeColor.RED);
		addGlass(47, DyeColor.RED);
		addGlass(48, DyeColor.LIGHT_BLUE);
		addGlass(49, DyeColor.LIGHT_BLUE);
		addGlass(50, DyeColor.LIGHT_BLUE);
		addGlass(51, DyeColor.RED);
		addGlass(52, DyeColor.RED);
		addGlass(53, DyeColor.RED);
	}
	
	@Override
	public void openInventory(Player player)
	{
		GameChooserListener listener = new GameChooserListener(player);
		listener.registerEvents();
		super.openInventory(player);
	}
}
