package co.pixelcade.core.menu.gamechooser;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import co.pixelcade.core.common.Module;

public class GameChooserListener extends Module {

	private Player player;
	
	public GameChooserListener(Player player)
	{
		this.player = player;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getInventory().getName().contains("PixelCade Games") && event.getWhoClicked() == player)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event)
	{
		if (event.getInventory().getName().contains("PixelCade Games"))
		{
			HandlerList.unregisterAll(this);
		}
	}
	
}
