package co.pixelcade.core.uuid;

import java.util.Arrays;
import java.util.UUID;

import co.pixelcade.core.Core;

public class MojangUUID {
	public static String getUUIDFromName(String username){
		try {
			return new UUIDFetcher(Arrays.asList(username)).call().get(username).toString();
		} catch (Exception e) {
			Core.manageError(e);
		}
		return null;
	}
	public static String getNameFromUUID(String uuid){
		try {
			return new NameFetcher(Arrays.asList(UUID.fromString(uuid))).call().get(uuid).toString();
		} catch (Exception e) {
			Core.manageError(e);
		}
		return null;
	}
}
