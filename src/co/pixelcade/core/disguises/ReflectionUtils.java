package co.pixelcade.core.disguises;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Server;

public class ReflectionUtils {
	 
    private static String preClassB = "org.bukkit.craftbukkit";
    private static String preClassM = "net.minecraft.server";
    private static boolean forge = false;
 
    static {
        if(Bukkit.getServer()!=null) {
            if(Bukkit.getVersion().contains("MCPC")||Bukkit.getVersion().contains("Forge")) forge = true;
            Server server = Bukkit.getServer();
            Class<?> bukkitServerClass = server.getClass();
            String[] pas = bukkitServerClass.getName().split("\\.");
            if (pas.length == 5) {
                String verB = pas[3];
                preClassB += "."+verB;
            }
            try {
                Method getHandle = bukkitServerClass.getDeclaredMethod("getHandle");
                Object handle = getHandle.invoke(server);
                @SuppressWarnings("rawtypes")
				Class handleServerClass = handle.getClass();
                pas = handleServerClass.getName().split("\\.");
                if (pas.length == 5) {
                    String verM = pas[3];
                    preClassM += "."+verM;
                }
            } catch (Exception ignored) {
            }
        }
    }
 
    public static boolean isForge(){
        return forge;
    }
 
    public static RefClass getRefClass(String... classes){
        for (String className: classes) try {
            className = className
                    .replace("{cb}", preClassB)
                    .replace("{nms}", preClassM)
                    .replace("{nm}", "net.minecraft");
            return getRefClass(Class.forName(className));
        } catch (ClassNotFoundException ignored) {
        }
        throw new RuntimeException("no class found");
    }
 
    @SuppressWarnings("rawtypes")
	public static RefClass getRefClass(Class clazz) {
        return new RefClass(clazz);
    }
    
    public static class RefClass {
        private final Class<?> clazz;
 
        public Class<?> getRealClass() {
            return clazz;
        }
        private RefClass(Class<?> clazz) {
            this.clazz = clazz;
        }
 
        public boolean isInstance(Object object){
            return clazz.isInstance(object);
        }
 
        @SuppressWarnings("rawtypes")
		public RefMethod getMethod(String name, Object... types) {
            try {
                Class[] classes = new Class[types.length];
                int i=0; for (Object e: types) {
                    if (e instanceof Class) classes[i++] = (Class)e;
                    else if (e instanceof RefClass) classes[i++] = ((RefClass) e).getRealClass();
                    else classes[i++] = e.getClass();
                }
                try {
                    return new RefMethod(clazz.getMethod(name, classes));
                } catch (NoSuchMethodException ignored) {
                    return new RefMethod(clazz.getDeclaredMethod(name, classes));
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
 
        @SuppressWarnings("rawtypes")
		public RefConstructor getConstructor(Object... types) {
            try {
                Class[] classes = new Class[types.length];
                int i=0; for (Object e: types) {
                    if (e instanceof Class) classes[i++] = (Class)e;
                    else if (e instanceof RefClass) classes[i++] = ((RefClass) e).getRealClass();
                    else classes[i++] = e.getClass();
                }
                try {
                    return new RefConstructor(clazz.getConstructor(classes));
                } catch (NoSuchMethodException ignored) {
                    return new RefConstructor(clazz.getDeclaredConstructor(classes));
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
 
        @SuppressWarnings({ "rawtypes", "unused" })
		public RefMethod findMethod(Object... types) {
            Class[] classes = new Class[types.length];
            int t=0; for (Object e: types) {
                if (e instanceof Class) classes[t++] = (Class)e;
                else if (e instanceof RefClass) classes[t++] = ((RefClass) e).getRealClass();
                else classes[t++] = e.getClass();
            }
            List<Method> methods = new ArrayList<>();
            Collections.addAll(methods, clazz.getMethods());
            Collections.addAll(methods, clazz.getDeclaredMethods());
            findMethod: for (Method m: methods) {
                Class<?>[] methodTypes = m.getParameterTypes();
                if (methodTypes.length != classes.length) continue;
                for (int i=0; i<classes.length; i++) {
                    if (!classes.equals(methodTypes)) continue findMethod;
                    return new RefMethod(m);
                }
            }
            throw new RuntimeException("no such method");
        }
 
        public RefMethod findMethodByName(String... names) {
            List<Method> methods = new ArrayList<>();
            Collections.addAll(methods, clazz.getMethods());
            Collections.addAll(methods, clazz.getDeclaredMethods());
            for (Method m: methods) {
                for (String name: names) {
                    if (m.getName().equals(name)) {
                        return new RefMethod(m);
                    }
                }
            }
            throw new RuntimeException("no such method");
        }
        
        public RefMethod findMethodByReturnType(RefClass type) {
            return findMethodByReturnType(type.clazz);
        }
 
        @SuppressWarnings("rawtypes")
		public RefMethod findMethodByReturnType(Class type) {
            if (type==null) type = void.class;
            List<Method> methods = new ArrayList<>();
            Collections.addAll(methods, clazz.getMethods());
            Collections.addAll(methods, clazz.getDeclaredMethods());
            for (Method m: methods) {
                if (type.equals(m.getReturnType())) {
                    return new RefMethod(m);
                }
            }
            throw new RuntimeException("no such method");
        }
 
        @SuppressWarnings("rawtypes")
		public RefConstructor findConstructor(int number) {
            List<Constructor> constructors = new ArrayList<>();
            Collections.addAll(constructors, clazz.getConstructors());
            Collections.addAll(constructors, clazz.getDeclaredConstructors());
            for (Constructor m: constructors) {
                if (m.getParameterTypes().length == number) return new RefConstructor(m);
            }
            throw new RuntimeException("no such constructor");
        }
 
        public RefField getField(String name) {
            try {
                try {
                    return new RefField(clazz.getField(name));
                } catch (NoSuchFieldException ignored) {
                    return new RefField(clazz.getDeclaredField(name));
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
 
        public RefField findField(RefClass type) {
            return findField(type.clazz);
        }
 
        @SuppressWarnings("rawtypes")
		public RefField findField(Class type) {
            if (type==null) type = void.class;
            List<Field> fields = new ArrayList<>();
            Collections.addAll(fields, clazz.getFields());
            Collections.addAll(fields, clazz.getDeclaredFields());
            for (Field f: fields) {
                if (type.equals(f.getType())) {
                    return new RefField(f);
                }
            }
            throw new RuntimeException("no such field");
        }
    }
 
    public static class RefMethod {
        private final Method method;
 
        public Method getRealMethod(){
            return method;
        }
        
        public RefClass getRefClass(){
            return new RefClass(method.getDeclaringClass());
        }

        public RefClass getReturnRefClass(){
            return new RefClass(method.getReturnType());
        }
        private RefMethod (Method method) {
            this.method = method;
            method.setAccessible(true);
        }
        
        public RefExecutor of(Object e) {
            return new RefExecutor(e);
        }
 
        public Object call(Object... params) {
            try{
                return method.invoke(null,params);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
 
        public class RefExecutor {
            Object e;
            public RefExecutor(Object e) {
                this.e = e;
            }
 
            public Object call(Object... params) {
                try{
                    return method.invoke(e,params);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
 
    public static class RefConstructor {
        @SuppressWarnings("rawtypes")
		private final Constructor constructor;
 
        @SuppressWarnings("rawtypes")
		public Constructor getRealConstructor(){
            return constructor;
        }
 
        public RefClass getRefClass(){
            return new RefClass(constructor.getDeclaringClass());
        }
        @SuppressWarnings("rawtypes")
		private RefConstructor (Constructor constructor) {
            this.constructor = constructor;
            constructor.setAccessible(true);
        }
 
        public Object create(Object... params) {
            try{
                return constructor.newInstance(params);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
 
    public static class RefField {
        private Field field;
 
        public Field getRealField(){
            return field;
        }
 
        public RefClass getRefClass(){
            return new RefClass(field.getDeclaringClass());
        }
 
        public RefClass getFieldRefClass(){
            return new RefClass(field.getType());
        }
        private RefField (Field field) {
            this.field = field;
            field.setAccessible(true);
        }
 
        public RefExecutor of(Object e) {
            return new RefExecutor(e);
        }
        public class RefExecutor {
            Object e;
            public RefExecutor(Object e) {
                this.e = e;
            }
 
            public void set(Object param) {
                try{
                    field.set(e,param);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
 
            public Object get() {
                try{
                    return field.get(e);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
 
}