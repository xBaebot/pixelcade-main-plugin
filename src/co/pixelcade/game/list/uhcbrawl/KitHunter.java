package co.pixelcade.game.list.uhcbrawl;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import co.pixelcade.game.kits.Kit;

public class KitHunter extends Kit {

	protected KitHunter() {
		super("Hunter");
		
		addDescriptionLine("With your trusty bow and arrows, you can claim those pesky sheep for your dinner!");
		addDescriptionLine("But sadly, you don't have the courage to use the bow and arrow on people!");
		
		ItemStack bow = new ItemStack(Material.BOW);
		ItemMeta bowMeta = bow.getItemMeta();
		bowMeta.setDisplayName("�aHunter's Bow");
		bow.setDurability((short) 250);
		bow.setItemMeta(bowMeta);
		
		addItem(0, bow);
		addItem(1, new ItemStack(Material.ARROW, 25));
	}
	
}
