package co.pixelcade.game.list.uhcbrawl;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

import co.pixelcade.core.Core;
import co.pixelcade.core.common.Message;
import co.pixelcade.game.Games;
import co.pixelcade.game.Power;

public class UHCBrawlGame extends Power{

	private int time = 8;
	private BukkitRunnable counterTask;
	
	private boolean pvp = false;
	
	public UHCBrawlGame() {
		super(Games.UHCBRAWL);
		Bukkit.getPluginManager().registerEvents(this, Core.instance);
		time();
	}
	
	public void time(){
		counterTask = new BukkitRunnable(){
			public void run(){
				Message.broadcast(time + " minutes remaining!");
				time--;
				
				if (time == 4) {
					pvp();
				}
				
				if (time == 0)
				{
					stop();
				}
				
			}
		};
		counterTask.runTaskTimer(Bukkit.getPluginManager().getPlugin("Core"), 0L, 1200L);
	}
	
	@EventHandler
	public void onPVPEvent(EntityDamageByEntityEvent event)
	{
		if (pvp)
		{
			event.setCancelled(false);
			return;
		}
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		event.setCancelled(false);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		event.setCancelled(false);
	}
	
	public void pvp(){
		this.pvp = true;
		Message.broadcast("Alert! PvP is now active!");
	}
	
	private void baseStop()
	{
		counterTask.cancel();
		HandlerList.unregisterAll(this);
	}
	
	public void stop()
	{
		baseStop();
		Message.broadcast("Game time has reached 00:00!");
	}
	
	public void forceStop(Player player)
	{
		baseStop();
		Message.broadcast("                                                        ");
	}
	
}
