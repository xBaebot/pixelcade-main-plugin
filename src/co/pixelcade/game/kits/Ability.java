package co.pixelcade.game.kits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import co.pixelcade.core.common.Message;
import co.pixelcade.core.common.Message.MessagePrefix;
import co.pixelcade.core.common.Module;

public abstract class Ability extends Module {

	private String name;
	private Material material;
	private List<Player> players;
	private int time;
	
	public Ability(String name, Material material, int time) {
		this.name = name;
		this.material = material;
		this.time = time;
		players = new ArrayList<Player>();
		
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	private HashMap<Player, BukkitRunnable> cooldownTask;
	private HashMap<Player, Integer> cooldownTime;
	
	public void startCooldown(final Player player) {
		cooldownTask.put(player, new BukkitRunnable()
				{
			private int i = time;
			
					@Override
					public void run() {
						if (i == 0) {
							Message.sendMessage(player, MessagePrefix.GAME, "You may now use " + name + "again!");
							cooldownTime.remove(player);
							cooldownTask.remove(player);
							cancel();
							return;
						}
						cooldownTime.put(player, i);
						i--;
					}
			
				});
	}
	
	public boolean cooldownCheck(Player player) {
		if (cooldownTime.containsKey(player)) {
			Message.sendMessage(player, MessagePrefix.GAME, "You need to wait " + cooldownTime.get(player) + " to use that.");
			return false;
		}
		return true;
	}
	
}
