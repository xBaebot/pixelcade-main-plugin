package co.pixelcade.game.kits.abilities;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

import co.pixelcade.game.kits.Ability;

public class AbilitySuperJump extends Ability {

	public AbilitySuperJump() {
		super("Super Jump", Material.FEATHER, 8);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction().toString().contains("RIGHT_CLICK") && getPlayers().contains(event.getPlayer()) && event.getPlayer().getItemInHand().getType() == Material.FEATHER) {
			boolean cooldown = cooldownCheck(event.getPlayer());
			if (cooldown == false) {
				return;
			}
			event.getPlayer().setVelocity(event.getPlayer().getEyeLocation().getDirection().multiply(1.8D));
			startCooldown(event.getPlayer());
		}
	}

}
