package co.pixelcade.game.kits;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Kit {
	
	private String name;
	private String[] description;
	private Ability[] abilities;
	private HashMap<Integer, ItemStack> items;
	
	public Kit(String name) {
		this.name = name;
		this.description = null;
		this.abilities = null;
		this.items = new HashMap<>();
	}
	
	public void addDescriptionLine(String line) {
		if (this.description == null) {
			this.description = new String[]{line};
			return;
		}
		
		description[description.length + 1] = line;
		return;
	}
	
	public void addAbility(Ability ability) {
		if (this.abilities == null) {
			this.abilities = new Ability[]{ability};
			return;
		}
		
		abilities[abilities.length + 1] = ability;
		return;
	}
	
	public void applyPlayer(Player player) {
		player.sendMessage("�cKit: �7�l" + name);
		player.sendMessage(" �7" + description[0]);
		player.sendMessage(" �7" + description[1]);
	}
	
	public void addItem(int slot, ItemStack item) {
		items.put(slot, item);
	}
	
}