package co.pixelcade.game;

import co.pixelcade.core.common.Module;

public abstract class Power extends Module{

	public Games game;
	
	public Power(Games game) {
		this.game = game;
		registerEvents();
	}
	
	public Games getGame(){
		return this.game;
	}
}
