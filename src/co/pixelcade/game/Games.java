package co.pixelcade.game;

public enum Games {
	
	UHCBRAWL("UHC Brawl"),
	FLOODESCAPE("Flood Escape"),
	MINECRAFTHEROES("Minecraft Heroes");
	
	String gameName;
	
	Games(String gameName){
		this.gameName = gameName;
	}
	
	public String getGameName(){
		return this.gameName;
	}
}
