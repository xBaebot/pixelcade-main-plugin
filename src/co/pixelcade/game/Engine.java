package co.pixelcade.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Engine {

	private GameEngine core;

	private static Engine instance = new Engine();
	
	public static Engine getInstance(){
		return instance;
	}
	
	public void start(){
		//NUMMMMM NUM NUM NUMMN UMMMMMMM
	
		core = new GameEngine();	
		core.start();
	}
	
	public void countdown(){
		BukkitRunnable task = new BukkitRunnable(){
			int time = 15;
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					player.setLevel(time);
					if (time == 0){
						player.setLevel(0);
					}
				}
				if (time == 0) {
					start();
				}
				time--;
			}
		};
		task.runTaskTimer(Bukkit.getPluginManager().getPlugin("Core"), 0L, 20L);
		}
	
	public GameEngine getEngine(){
		return core;
	}
	
}
