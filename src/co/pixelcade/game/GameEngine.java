package co.pixelcade.game;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import co.pixelcade.core.api.Title;
import co.pixelcade.core.common.Message;
import co.pixelcade.game.list.uhcbrawl.UHCBrawlGame;

public class GameEngine {
	
	private Games currentGame;
	private Games nextGame;
	
	private UHCBrawlGame uhcBrawlGame;
	
	public ArrayList<Player> alive = new ArrayList<Player>();
	public ArrayList<Player> dead = new ArrayList<Player>();
	
	public GameEngine(){
		this.currentGame = Games.UHCBRAWL;
		this.nextGame = null;
	}
	
	public void register(){
		if (currentGame == Games.UHCBRAWL){
			uhcBrawlGame = new UHCBrawlGame();
		}
	}
	
	public void unregister(){
		if (currentGame == Games.UHCBRAWL){
			uhcBrawlGame.stop();
			currentGame = null;
			uhcBrawlGame = null;
		}
	}
	
	public void sendToGame(){
		
	}
	
	public void start(){
		Message.broadcast("The game has started!");

		for (Player player : Bukkit.getOnlinePlayers()){
			new Title(player, ChatColor.AQUA + currentGame.getGameName(), "has started!");
		}
		
		alert("Game has STARTED");
	
		sendToGame();
		
		register();
	}
	
	public void end(){
		alert("Game has ENDED");
	
		this.alive.clear();
		this.dead.clear();
	}
	
	public void alert(String alert){
		System.out.println("       ");
		System.out.println(" - Game Engine Alert - ");
		System.out.println(alert);
		System.out.println("       ");
	}
	
	public void rotate(){
		
	}
}
