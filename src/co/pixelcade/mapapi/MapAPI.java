package co.pixelcade.mapapi;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

import co.pixelcade.core.Core;
import co.pixelcade.core.common.Message;

public class MapAPI{

	private File file;
	private YamlConfiguration config;
	
	public MapAPI(){
		this.file = new File(Core.instance.getDataFolder() + "/gamemap/", "mapconfig.yml");
		
		if (!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				Message.log("File Generation for 'mapconfig.yml' failed! Check below");
				e.printStackTrace();
			}
		}
		
		config = YamlConfiguration.loadConfiguration(file);
	
		register();
	}
		
	public void register(){
		File mapList = new File(Core.instance.getDataFolder() + "/gamemap/maps", "mapList.yml");
		
		if (!mapList.exists()){
			try{
				mapList.createNewFile();
			} catch (IOException e){
				Message.log("File Generation for 'mapList.yml' failed! Check below");
				e.printStackTrace();
			}
		}
	}
}
