# README #

### What is this repository for? ###

* This has the main pixelcade plugin - it has our core API, our hub plugin, game engine and anything else we want.
* Version: 1.3

### Current Todo list ###

* Kit System for game engine [not completed] {Oli}
* Plugin updater - seperate plugin to automatically update the Main plugin from 1 main folder. [not completed] {Zeh & Alex}
* Punishment System (talk to Alex for design) [not completed] {Zeh}
* NPC System with names showing all the time and multi line names {Oli & Zeh}

### Who do I talk to? ###

* To find out what to do, contact Alex or SenseiMonkey
* To find out about how to do something/something to do with the code, contact Oli.